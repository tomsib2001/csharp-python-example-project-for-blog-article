﻿using static Blockchain.Identities;
using static fa2.initialBlockchainStorage;
using static FactoriTypes.Types;
using System.Numerics;

async static Task main()
{
    var storage = initial_blockchain_storage();
    storage.administrator = new fa2.Constructor_operator(aliceFlextesa.Address);
    var kt1 = await fa2.Functions.Deploy(aliceFlextesa, storage, 1000000, 100000, 30000, 1000000, "ghostnet", false);
    if (kt1 == null)
    {
        Console.WriteLine("Deployment seems to have failed, no KT1 in output.");
        return;
    }
    fa2.Token_id amount = new fa2.Token_id(new BigInteger(1000));
    fa2.Token_id token_id = new fa2.Token_id(new BigInteger(0));
    fa2.Constructor_operator addr = new fa2.Constructor_operator(aliceFlextesa.Address);
    fa2.Mint mintParam = new fa2.Mint(amount, addr, new Map<fa2.K, fa2.V>(), token_id);
    await Task.Delay(16000); // wait 16 seconds
    var ophash = await fa2.Functions.CallMint(aliceFlextesa, kt1, mintParam, 1000000, 100000, 30000, 1000000, "ghostnet", false);
    Console.WriteLine($"Ophash: {ophash}");
}

await main();