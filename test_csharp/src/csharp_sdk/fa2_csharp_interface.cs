using FactoriTypes;
using System.Numerics;
using Netezos.Encoding;
using Netezos.Rpc;
using Netezos.Keys;
namespace fa2
{
    public class Token_id : Types.Nat
    {
        public Token_id(Types.Nat v) : base(v) { }
        public static implicit operator BigInteger(Token_id a) => (BigInteger)(Types.Nat)a;
        public static implicit operator Token_id(BigInteger a) => (Token_id)(Types.Nat)a;
    }



    public class Constructor_operator : Types.Address
    {
        public Constructor_operator(Types.Address v) : base(v) { }
        public static implicit operator string(Constructor_operator a) => (string)(Types.Address)a;
        public static implicit operator Constructor_operator(string a) => (Constructor_operator)(Types.Address)a;
    }



    public record Add_operator
    {
        public Constructor_operator owner { get; set; }
        public Constructor_operator _operator { get; set; }
        public Token_id token_id { get; set; }
        public Add_operator(Constructor_operator owner, Constructor_operator _operator, Token_id token_id)
        {
            this.owner = owner;
            this._operator = _operator;
            this.token_id = token_id;
        }
    }



    public class Type0 { }
    class Type0_Remove_operator_constructor_subtype : Type0
    {
        public string kind = "Remove_operator_constructor";
        public Add_operator? Remove_operator_element { get; set; }
    }

    class Type0_Add_operator_constructor_subtype : Type0
    {
        public string kind = "Add_operator_constructor";
        public Add_operator? Add_operator_element { get; set; }
    }






    public class Update_operators : List<Type0> { }





    public record Type1
    {
        public Constructor_operator to_ { get; set; }
        public Token_id token_id { get; set; }
        public Token_id amount { get; set; }
        public Type1(Constructor_operator to_, Token_id token_id, Token_id amount)
        {
            this.to_ = to_;
            this.token_id = token_id;
            this.amount = amount;
        }
    }



    public record Type2
    {
        public Constructor_operator from_ { get; set; }
        public List<Type1> txs { get; set; }
        public Type2(Constructor_operator from_, List<Type1> txs)
        {
            this.from_ = from_;
            this.txs = txs;
        }
    }



    public class Transfer : List<Type2> { }





    public class Set_pause : Types.Bool
    {
        public Set_pause(Types.Bool v) : base(v) { }
        public static implicit operator bool(Set_pause a) => (bool)(Types.Bool)a;
        public static implicit operator Set_pause(bool a) => (Set_pause)(Types.Bool)a;
    }





    public class V : Types.Bytes
    {
        public V(Types.Bytes v) : base(v) { }
        public static implicit operator byte[](V a) => (byte[])(Types.Bytes)a;
        public static implicit operator V(byte[] a) => (V)(Types.Bytes)a;
    }



    public class K
    {
        private string _value;
        public K(string str)
        {
            _value = str;
        }
        public static implicit operator string(K v) => v._value;
        public static implicit operator K(string s) => new K(s);
    }



    public record Set_metadata
    {
        public K k { get; set; }
        public V v { get; set; }
        public Set_metadata(K k, V v)
        {
            this.k = k;
            this.v = v;
        }
    }





    public class Set_administrator : Constructor_operator
    {
        public Set_administrator(string addr) : base(addr) { }
        public static implicit operator string(Set_administrator a) => a;
        public static implicit operator Set_administrator(string a) => a;
    }





    public record Mint
    {
        public Token_id amount { get; set; }
        public Constructor_operator address { get; set; }
        public Types.Map<K, V> metadata { get; set; }
        public Token_id token_id { get; set; }
        public Mint(Token_id amount, Constructor_operator address, Types.Map<K, V> metadata, Token_id token_id)
        {
            this.amount = amount;
            this.address = address;
            this.metadata = metadata;
            this.token_id = token_id;
        }
    }





    public record Type6
    {
        public Constructor_operator owner { get; set; }
        public Token_id token_id { get; set; }
        public Type6(Constructor_operator owner, Token_id token_id)
        {
            this.owner = owner;
            this.token_id = token_id;
        }
    }



    public record Balance_of
    {
        public List<Type6> requests { get; set; }
        public Types.Contract callback { get; set; }
        public Balance_of(List<Type6> requests, Types.Contract callback)
        {
            this.requests = requests;
            this.callback = callback;
        }
    }





    public record Type8
    {
        public Token_id token_id { get; set; }
        public Types.Map<K, V> token_info { get; set; }
        public Type8(Token_id token_id, Types.Map<K, V> token_info)
        {
            this.token_id = token_id;
            this.token_info = token_info;
        }
    }



    public record Storage
    {
        public Set_pause paused { get; set; }
        public Types.BigMap<Add_operator, Types.Unit> operators { get; set; }
        public Token_id all_tokens { get; set; }
        public Constructor_operator administrator { get; set; }
        public Types.BigMap<Types.MyTuple<Constructor_operator, Token_id>, Token_id> ledger { get; set; }
        public Types.BigMap<K, V> metadata { get; set; }
        public Types.BigMap<Token_id, Type8> token_metadata { get; set; }
        public Types.BigMap<Token_id, Token_id> total_supply { get; set; }
        public Storage(Set_pause paused, Types.BigMap<Add_operator, Types.Unit> operators, Token_id all_tokens, Constructor_operator administrator, Types.BigMap<Types.MyTuple<Constructor_operator, Token_id>, Token_id> ledger, Types.BigMap<K, V> metadata, Types.BigMap<Token_id, Type8> token_metadata, Types.BigMap<Token_id, Token_id> total_supply)
        {
            this.paused = paused;
            this.operators = operators;
            this.all_tokens = all_tokens;
            this.administrator = administrator;
            this.ledger = ledger;
            this.metadata = metadata;
            this.token_metadata = token_metadata;
            this.total_supply = total_supply;
        }
    }



    public class Functions
    {
        public static Token_id token_idGenerator()
        {
            return (Token_id)new Token_id(Types.NatGenerator());
        }

        public static IMicheline token_idEncode(Token_id arg)
        {
            try
            {
                return Types.NatEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in token_idEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Token_id token_idDecode(IMicheline m) { return new Token_id((BigInteger)(Types.NatDecode(m))); }

        /*

        */

        public static Constructor_operator _operatorGenerator()
        {
            return (Constructor_operator)new Constructor_operator(Types.AddressGenerator());
        }

        public static IMicheline _operatorEncode(Constructor_operator arg)
        {
            try
            {
                return Types.AddressEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in _operatorEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Constructor_operator _operatorDecode(IMicheline m) { return new Constructor_operator((string)(Types.AddressDecode(m))); }

        /*

        */

        public static Add_operator add_operatorGenerator()
        {
            return new Add_operator(owner: _operatorGenerator(),
                _operator: _operatorGenerator(),
                token_id: token_idGenerator());
        }

        public static IMicheline add_operatorEncode(Add_operator arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (_operatorEncode(arg.owner)),
    (_operatorEncode(arg._operator)),
    (token_idEncode(arg.token_id))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in add_operatorEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Add_operator add_operatorDecode(IMicheline arg)
        {
            var before_projection = (Types.Tuple3Decode(_operatorDecode, _operatorDecode, token_idDecode))(arg);
            return new Add_operator(owner: (before_projection.Item1),
            _operator: (before_projection.Item2),
            token_id: (before_projection.Item3))
            ;
        }

        /*

        */

        public static Type0 type0Generator()
        {
            Type0_Add_operator_constructor_subtype res0 = new Type0_Add_operator_constructor_subtype();
            res0.kind = "Add_operator_constructor";
            res0.Add_operator_element = add_operatorGenerator();
            ;
            Type0_Remove_operator_constructor_subtype res1 = new Type0_Remove_operator_constructor_subtype();
            res1.kind = "Remove_operator_constructor";
            res1.Remove_operator_element = add_operatorGenerator();
            ;
            return Types.chooseFrom<Type0>(new List<Type0>{
res0,res1
});
        }

        public static IMicheline type0Encode(Type0 arg)
        {
            try
            {
                switch (arg)
                {
                    case (Type0_Add_operator_constructor_subtype localarg):
                        return Types.make_left(add_operatorEncode(localarg.Add_operator_element == null ? throw new EncodeError("null") : localarg.Add_operator_element));
                    case (Type0_Remove_operator_constructor_subtype localarg):
                        return Types.make_right(add_operatorEncode(localarg.Remove_operator_element == null ? throw new EncodeError("null") : localarg.Remove_operator_element));
                    default:
                        throw new EncodeError("Unknown case in sumtype_encode");
                }
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in type0Encode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Type0 type0Decode(IMicheline arg)
        {

            switch (arg)
            {//Left case

                case MichelinePrim mp_:
                    switch (mp_.Prim)
                    {
                        case PrimType.Left:
                            arg = mp_.Args[0];
                            Type0_Add_operator_constructor_subtype res = new Type0_Add_operator_constructor_subtype();
                            res.kind = "Add_operator_constructor";
                            res.Add_operator_element = add_operatorDecode(arg);
                            return res;

                            ;

                        case PrimType.Right:
                            break;
                        default:
                            throw new DecodeError($"{mp_} should be Left or Right");
                    }
                    break;
                default:
                    throw new DecodeError($"Could not decode {arg}, it should be a Prim");
            }
            switch (arg)
            {//Right case

                case MichelinePrim mp_:
                    switch (mp_.Prim)
                    {
                        case PrimType.Right:
                            arg = mp_.Args[0];
                            Type0_Remove_operator_constructor_subtype res = new Type0_Remove_operator_constructor_subtype();
                            res.kind = "Remove_operator_constructor";
                            res.Remove_operator_element = add_operatorDecode(arg);
                            return res;

                            ;
                        case PrimType.Left:
                            break;
                        default:
                            throw new DecodeError($"{mp_} should be Left or Right");
                    }
                    break;
                default:
                    throw new DecodeError($"Could not decode {arg}, it should be a Prim");
            };
            throw new DecodeError($"Could not identify any known sumtype path for this value: {arg}");
        }

        /*

        */

        public static Update_operators update_operatorsGenerator()
        {
            return (Update_operators)(Types.ListGenerator(type0Generator))();
        }

        public static IMicheline update_operatorsEncode(Update_operators arg)
        {
            try
            {
                return Types.ListEncode<Type0>(type0Encode)(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in update_operatorsEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Update_operators update_operatorsDecode(IMicheline arg) { return (Update_operators)(Types.ListDecode<Type0>(type0Decode))(arg); }

        /*

        */



        public static Type1 type1Generator()
        {
            return new Type1(to_: _operatorGenerator(),
                token_id: token_idGenerator(),
                amount: token_idGenerator());
        }

        public static IMicheline type1Encode(Type1 arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (_operatorEncode(arg.to_)),
    (token_idEncode(arg.token_id)),
    (token_idEncode(arg.amount))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in type1Encode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Type1 type1Decode(IMicheline arg)
        {
            var before_projection = (Types.Tuple3Decode(_operatorDecode, token_idDecode, token_idDecode))(arg);
            return new Type1(to_: (before_projection.Item1),
            token_id: (before_projection.Item2),
            amount: (before_projection.Item3))
            ;
        }

        /*

        */

        public static Type2 type2Generator()
        {
            return new Type2(from_: _operatorGenerator(),
                txs: (Types.ListGenerator<Type1>(type1Generator))());
        }

        public static IMicheline type2Encode(Type2 arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (_operatorEncode(arg.from_)),
    (Types.ListEncode<Type1>(type1Encode)(arg.txs))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in type2Encode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Type2 type2Decode(IMicheline arg)
        {
            var before_projection = (Types.Tuple2Decode(_operatorDecode, Types.ListDecode(type1Decode)))(arg);
            return new Type2(from_: (before_projection.Item1),
            txs: (before_projection.Item2))
            ;
        }

        /*

        */

        public static Transfer transferGenerator()
        {
            return (Transfer)(Types.ListGenerator(type2Generator))();
        }

        public static IMicheline transferEncode(Transfer arg)
        {
            try
            {
                return Types.ListEncode<Type2>(type2Encode)(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in transferEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Transfer transferDecode(IMicheline arg) { return (Transfer)(Types.ListDecode<Type2>(type2Decode))(arg); }

        /*

        */



        public static Set_pause set_pauseGenerator()
        {
            return (Set_pause)new Set_pause(Types.BoolGenerator());
        }

        public static IMicheline set_pauseEncode(Set_pause arg)
        {
            try
            {
                return Types.BoolEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in set_pauseEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Set_pause set_pauseDecode(IMicheline m) { return new Set_pause((bool)(Types.BoolDecode(m))); }

        /*

        */



        public static V vGenerator()
        {
            return (V)new V(Types.BytesGenerator());
        }

        public static IMicheline vEncode(V arg)
        {
            try
            {
                return Types.BytesEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in vEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static V vDecode(IMicheline m) { return new V((byte[])(Types.BytesDecode(m))); }

        /*

        */

        public static K kGenerator()
        {
            return (K)new K(Types.StringGenerator());
        }

        public static IMicheline kEncode(K arg)
        {
            try
            {
                return Types.StringEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in kEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static K kDecode(IMicheline m) { return (Types.StringDecode(m)); }

        /*

        */

        public static Set_metadata set_metadataGenerator()
        {
            return new Set_metadata(k: kGenerator(),
                v: vGenerator());
        }

        public static IMicheline set_metadataEncode(Set_metadata arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (kEncode(arg.k)),
    (vEncode(arg.v))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in set_metadataEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Set_metadata set_metadataDecode(IMicheline arg)
        {
            var before_projection = (Types.Tuple2Decode(kDecode, vDecode))(arg);
            return new Set_metadata(k: (before_projection.Item1),
            v: (before_projection.Item2))
            ;
        }

        /*

        */




        public static Set_administrator set_administratorGenerator()
        {
            return (Set_administrator)_operatorGenerator();
        }


        public static IMicheline set_administratorEncode(Set_administrator arg)
        {
            try
            {
                return _operatorEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in set_administratorEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }


        public static Set_administrator set_administratorDecode(IMicheline arg) { return (Set_administrator)_operatorDecode(arg); }

        /*

        */



        public static Mint mintGenerator()
        {
            return new Mint(amount: token_idGenerator(),
                address: _operatorGenerator(),
                metadata: (Types.MapGenerator<K, V>(kGenerator, vGenerator))(),
                token_id: token_idGenerator());
        }

        public static IMicheline mintEncode(Mint arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (_operatorEncode(arg.address)),
    (token_idEncode(arg.amount))
}
},
    (Types.MapEncode<K,V>(kEncode,vEncode)(arg.metadata)),
    (token_idEncode(arg.token_id))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in mintEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Mint mintDecode(IMicheline arg)
        {
            var before_projection = (Types.Tuple3Decode((Types.Tuple2Decode(_operatorDecode, token_idDecode)), Types.MapDecode(kDecode, vDecode), token_idDecode))(arg);
            return new Mint(address: (before_projection.Item1.Item1),
            amount: (before_projection.Item1.Item2),
            metadata: (before_projection.Item2),
            token_id: (before_projection.Item3))
            ;
        }

        /*

        */



        public static Type6 type6Generator()
        {
            return new Type6(owner: _operatorGenerator(),
                token_id: token_idGenerator());
        }

        public static IMicheline type6Encode(Type6 arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (_operatorEncode(arg.owner)),
    (token_idEncode(arg.token_id))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in type6Encode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Type6 type6Decode(IMicheline arg)
        {
            var before_projection = (Types.Tuple2Decode(_operatorDecode, token_idDecode))(arg);
            return new Type6(owner: (before_projection.Item1),
            token_id: (before_projection.Item2))
            ;
        }

        /*

        */

        public static Balance_of balance_ofGenerator()
        {
            return new Balance_of(requests: (Types.ListGenerator<Type6>(type6Generator))(),
                callback: Types.ContractGenerator());
        }

        public static IMicheline balance_ofEncode(Balance_of arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (Types.ListEncode<Type6>(type6Encode)(arg.requests)),
    (Types.ContractEncode(arg.callback))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in balance_ofEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Balance_of balance_ofDecode(IMicheline arg)
        {
            var before_projection = (Types.Tuple2Decode(Types.ListDecode(type6Decode), Types.ContractDecode))(arg);
            return new Balance_of(requests: (before_projection.Item1),
            callback: (before_projection.Item2))
            ;
        }

        /*

        */



        public static Type8 type8Generator()
        {
            return new Type8(token_id: token_idGenerator(),
                token_info: (Types.MapGenerator<K, V>(kGenerator, vGenerator))());
        }

        public static IMicheline type8Encode(Type8 arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (token_idEncode(arg.token_id)),
    (Types.MapEncode<K,V>(kEncode,vEncode)(arg.token_info))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in type8Encode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Type8 type8Decode(IMicheline arg)
        {
            var before_projection = (Types.Tuple2Decode(token_idDecode, Types.MapDecode(kDecode, vDecode)))(arg);
            return new Type8(token_id: (before_projection.Item1),
            token_info: (before_projection.Item2))
            ;
        }

        /*

        */

        public static Storage storageGenerator()
        {
            return new Storage(paused: set_pauseGenerator(),
                operators: (Types.BigMapGenerator<Add_operator, Types.Unit>(add_operatorGenerator, Types.UnitGenerator))(),
                all_tokens: token_idGenerator(),
                administrator: _operatorGenerator(),
                ledger: (Types.BigMapGenerator<Types.MyTuple<Constructor_operator, Token_id>, Token_id>((Types.Tuple2Generator<Constructor_operator, Token_id>(_operatorGenerator,
        token_idGenerator)), token_idGenerator))(),
                metadata: (Types.BigMapGenerator<K, V>(kGenerator, vGenerator))(),
                token_metadata: (Types.BigMapGenerator<Token_id, Type8>(token_idGenerator, type8Generator))(),
                total_supply: (Types.BigMapGenerator<Token_id, Token_id>(token_idGenerator, token_idGenerator))());
        }

        public static IMicheline storageEncode(Storage arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (_operatorEncode(arg.administrator)),
    (token_idEncode(arg.all_tokens))
}
},
    (Types.BigMapEncode<Types.MyTuple<Constructor_operator,Token_id>,Token_id>(Types.Tuple2Encode<Constructor_operator,Token_id>(_operatorEncode,token_idEncode),token_idEncode)(arg.ledger)),
    (Types.BigMapEncode<K,V>(kEncode,vEncode)(arg.metadata))
}
},
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (Types.BigMapEncode<Add_operator,Types.Unit>(add_operatorEncode,Types.UnitEncode)(arg.operators)),
    (set_pauseEncode(arg.paused))
}
},
    (Types.BigMapEncode<Token_id,Type8>(token_idEncode,type8Encode)(arg.token_metadata)),
    (Types.BigMapEncode<Token_id,Token_id>(token_idEncode,token_idEncode)(arg.total_supply))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in storageEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Storage storageDecode(IMicheline arg)
        {
            var before_projection = (Types.Tuple4Decode((Types.Tuple3Decode((Types.Tuple2Decode(_operatorDecode, token_idDecode)), Types.BigMapDecode(Types.Tuple2Decode<Constructor_operator, Token_id>(_operatorDecode, token_idDecode), token_idDecode), Types.BigMapDecode(kDecode, vDecode))), (Types.Tuple2Decode(Types.BigMapDecode(add_operatorDecode, Types.UnitDecode), set_pauseDecode)), Types.BigMapDecode(token_idDecode, type8Decode), Types.BigMapDecode(token_idDecode, token_idDecode)))(arg);
            return new Storage(administrator: (before_projection.Item1.Item1.Item1),
            all_tokens: (before_projection.Item1.Item1.Item2),
            ledger: (before_projection.Item1.Item2),
            metadata: (before_projection.Item1.Item3),
            operators: (before_projection.Item2.Item1),
            paused: (before_projection.Item2.Item2),
            token_metadata: (before_projection.Item3),
            total_supply: (before_projection.Item4))
            ;
        }

        /*

        */

        public static async Task<string?> Deploy(Netezos.Keys.Key from, Storage initial_storage, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var encoded_initial_storage = storageEncode(initial_storage);
            var options = new System.Text.Json.JsonSerializerOptions();
            options.Converters.Add(new Netezos.Encoding.Serialization.MichelineConverter());
            string json1 = System.Text.Json.JsonSerializer.Serialize(encoded_initial_storage, options);
            if (debug)
            {
                Console.WriteLine(json1);
            }
            string? kt1 = await Blockchain.Functions.DeployContract("src/csharp_sdk/fa2_code.json", from, encoded_initial_storage, fee, gasLimit, storageLimit, amount, networkName, debug);
            return kt1;
        }










        public static async Task<string?> CallUpdate_operators(Netezos.Keys.Key from, string kt1, Update_operators param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = update_operatorsEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = update_operatorsEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "update_operators", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to update_operators: {opHash}");
            else { Console.WriteLine("Failed call to update_operators"); }
            return opHash;
        }







        public static async Task<string?> CallTransfer(Netezos.Keys.Key from, string kt1, Transfer param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = transferEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = transferEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "transfer", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to transfer: {opHash}");
            else { Console.WriteLine("Failed call to transfer"); }
            return opHash;
        }



        public static async Task<string?> CallSet_pause(Netezos.Keys.Key from, string kt1, Set_pause param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = set_pauseEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = set_pauseEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "set_pause", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to set_pause: {opHash}");
            else { Console.WriteLine("Failed call to set_pause"); }
            return opHash;
        }







        public static async Task<string?> CallSet_metadata(Netezos.Keys.Key from, string kt1, Set_metadata param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = set_metadataEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = set_metadataEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "set_metadata", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to set_metadata: {opHash}");
            else { Console.WriteLine("Failed call to set_metadata"); }
            return opHash;
        }



        public static async Task<string?> CallSet_administrator(Netezos.Keys.Key from, string kt1, Set_administrator param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = set_administratorEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = set_administratorEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "set_administrator", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to set_administrator: {opHash}");
            else { Console.WriteLine("Failed call to set_administrator"); }
            return opHash;
        }



        public static async Task<string?> CallMint(Netezos.Keys.Key from, string kt1, Mint param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = mintEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = mintEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "mint", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to mint: {opHash}");
            else { Console.WriteLine("Failed call to mint"); }
            return opHash;
        }





        public static async Task<string?> CallBalance_of(Netezos.Keys.Key from, string kt1, Balance_of param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = balance_ofEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = balance_ofEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "balance_of", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to balance_of: {opHash}");
            else { Console.WriteLine("Failed call to balance_of"); }
            return opHash;
        }





    }


    public class initialBlockchainStorage
    {

        public static Storage initial_blockchain_storage()
        {
            return (Storage)(new(administrator: new Constructor_operator("KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM"),
            all_tokens: new Token_id(System.Numerics.BigInteger.Parse("42")),
            ledger: (Types.BigMap<Types.MyTuple<Constructor_operator, Token_id>, Token_id>)new Types.AbstractBigMap<Types.MyTuple<Constructor_operator, Token_id>, Token_id>(System.Numerics.BigInteger.Parse("139881")),
            metadata: (Types.BigMap<K, V>)new Types.AbstractBigMap<K, V>(System.Numerics.BigInteger.Parse("139882")),
            operators: (Types.BigMap<Add_operator, Types.Unit>)new Types.AbstractBigMap<Add_operator, Types.Unit>(System.Numerics.BigInteger.Parse("139883")),
            paused: new Set_pause(false),
            token_metadata: (Types.BigMap<Token_id, Type8>)new Types.AbstractBigMap<Token_id, Type8>(System.Numerics.BigInteger.Parse("139884")),
            total_supply: (Types.BigMap<Token_id, Token_id>)new Types.AbstractBigMap<Token_id, Token_id>(System.Numerics.BigInteger.Parse("139885"))));
        }
        /* {"prim":"Pair","args":[{"prim":"Pair","args":[{"prim":"Pair","args":[{"string":"KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM"},{"int":"42"}]},{"int":"139881"},{"int":"139882"}]},{"prim":"Pair","args":[{"int":"139883"},{"prim":"False"}]},{"int":"139884"},{"int":"139885"}]}
        VRecord((Pair_type.PairP
           [(Pair_type.LeafP
               ({ Factori_utils.original = "%administrator";
                  sanitized = "administrator"; accessors = [] },
                VAddress KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%all_tokens"; sanitized = "all_tokens";
                   accessors = [] },
                 VInt 42));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%ledger"; sanitized = "ledger";
                   accessors = [] },
                 VBigMap 139881));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%metadata"; sanitized = "metadata";
                   accessors = [] },
                 VBigMap 139882));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%operators"; sanitized = "operators";
                   accessors = [] },
                 VBigMap 139883));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%paused"; sanitized = "paused";
                   accessors = [] },
                 VBool false));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%token_metadata";
                   sanitized = "token_metadata"; accessors = [] },
                 VBigMap 139884));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%total_supply";
                   sanitized = "total_supply"; accessors = [] },
                 VBigMap 139885))
             ]))*/


    }
}