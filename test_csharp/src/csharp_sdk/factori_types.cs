
using Netezos.Encoding;
using System.Numerics;
using System.Text;


namespace FactoriTypes
{

    public class EncodeError : Exception
    {
        public EncodeError()
        {
        }

        public EncodeError(string msg) : base(msg) { }
    }

    public class DecodeError : Exception
    {
        public DecodeError()
        {
        }

        public DecodeError(string msg) : base(msg) { }
    }

    public class HandleErrors
    {

        public void HandleEncodingException(EncodeError e)
        {
            Console.WriteLine($"Encoding error: {e}");
        }

        public void HandleDecodingException(EncodeError e)
        {
            Console.WriteLine($"Decoding error: {e}");
        }

    }

    public class Utils
    {
        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }

    // Our type for ints is System.Numerics.BigInteger
    // We could replace MichelineInt by IMicheline, but it's better to have the finer type
    public class Types
    {

        public static bool is_left(IMicheline im)
        {
            switch (im)
            {
                case MichelinePrim mp:
                    switch (mp.Prim)
                    {
                        case PrimType.Left:
                            return true;
                        case PrimType.Right:
                            return false;
                        default:
                            throw new DecodeError($"There was an error, we are trying to understand whether {im} is Left");
                    }
                default:
                    throw new DecodeError($"There was an error, we are trying to understand whether {im} is Left");
            }
        }

        public static bool is_right(IMicheline im)
        {
            switch (im)
            {
                case MichelinePrim mp:
                    switch (mp.Prim)
                    {
                        case PrimType.Left:
                            return false;
                        case PrimType.Right:
                            return true;
                        default:
                            throw new DecodeError($"There was an error, we are trying to understand whether {im} is Right");
                    }
                default:
                    throw new DecodeError($"There was an error, we are trying to understand whether {im} is Right");
            }
        }

        public class Map<TKey, TValue> : Dictionary<TKey, TValue>
        where TKey : notnull
        where TValue : notnull
        {
            public Map() : base() { }
            public Map(Dictionary<TKey, TValue> dict) : base(dict) { }
        }

        public static Func<Dictionary<TKey, TValue>, IMicheline> DictEncode<TKey, TValue>(Func<TKey, IMicheline> keyEncode, Func<TValue, IMicheline> valueEncode) where TKey : notnull where TValue : notnull
        {
            return (Dictionary<TKey, TValue> x) =>
                {
                    var res = new MichelineArray(x.Keys.Count());

                    foreach (KeyValuePair<TKey, TValue> kvPair in x)
                    {
                        var elem = new MichelinePrim
                        {
                            Prim = PrimType.Elt,
                            Args = new List<IMicheline>{
                                keyEncode(kvPair.Key),
                                valueEncode(kvPair.Value)
                            }
                        };
                        res.Append(elem);

                    };
                    return res;
                };

        }

        public static Func<Map<TKey, TValue>, IMicheline> MapEncode<TKey, TValue>(Func<TKey, IMicheline> keyEncode, Func<TValue, IMicheline> valueEncode) where TKey : notnull where TValue : notnull
        {
            return DictEncode<TKey, TValue>(keyEncode, valueEncode);
        }


        public static Func<IMicheline, KeyValuePair<TKey, TValue>> DecodeMapElt<TKey, TValue>(Func<IMicheline, TKey> keyDecode, Func<IMicheline, TValue> valueDecode) where TKey : notnull where TValue : notnull
        {
            return (IMicheline im) =>
            {
                switch (im)
                {
                    case MichelinePrim mp:
                        switch (mp.Prim)
                        {
                            case PrimType.Elt:
                                if (mp.Args.Count < 2)
                                {
                                    throw new DecodeError($"There should be two elements in Elt component {mp} of {im}");
                                }
                                else
                                {
                                    return new KeyValuePair<TKey, TValue>(keyDecode(mp.Args[0]), valueDecode(mp.Args[1]));
                                }
                            default:
                                throw new DecodeError($"Error trying to decode a map element from {im}, {mp} is not an Elt");
                        }
                    default:
                        throw new DecodeError($"Error trying to decode a map element from {im}");

                }
            };
        }

        public static Func<IMicheline, Map<TKey, TValue>> MapDecode<TKey, TValue>(Func<IMicheline, TKey> keyDecode, Func<IMicheline, TValue> valueDecode) where TKey : notnull where TValue : notnull
        {
            return (IMicheline im) =>
            {
                var res = new Dictionary<TKey, TValue>();
                switch (im)
                {
                    case MichelineArray ma:
                        foreach (var item in ma)
                        {
                            res.Append(DecodeMapElt<TKey, TValue>(keyDecode, valueDecode)(item));
                        }
                        return (Map<TKey, TValue>)res;
                    default:
                        throw new DecodeError($"Error trying to decode a map from {im}");
                }
            };
        }

        public static Func<IMicheline, List<T>> ListDecode<T>(Func<IMicheline, T> tDecode)
        {
            return (IMicheline im) =>
            {
                switch (im)
                {
                    case MichelineArray ma:
                        List<T> res = new List<T>();
                        foreach (var item in ma)
                        {
                            res.Append(tDecode(item));
                        }
                        return res;
                    default:
                        throw new DecodeError($"Error decoding {im} as a list");
                }
            };
        }

        public static Func<IMicheline, Set<T>> SetDecode<T>(Func<IMicheline, T> tDecode)
        {
            return (IMicheline im) =>
            {
                switch (im)
                {
                    case MichelineArray ma:
                        Set<T> res = new Set<T>();
                        foreach (var item in ma)
                        {
                            res.Append(tDecode(item));
                        }
                        return res;
                    default:
                        throw new DecodeError($"Error decoding {im} as a set");
                }
            };
        }


        public static Lambda LambdaDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelinePrim mp:
                    switch (mp.Prim)
                    {
                        case PrimType.LAMBDA:
                            return new Lambda();
                        default:
                            throw new DecodeError($"Could not decode lambda {im} (Prim but not LAMBDA)");
                    }
                default:
                    throw new DecodeError($"Could not decode lambda {im} (Not Prim)");
            }
        }

        public static Func<Dictionary<TKey, TValue>> DictionaryGenerator<TKey, TValue>(Func<TKey> keyGenerator, Func<TValue> valueGenerator) where TKey : notnull where TValue : notnull
        {
            return () =>
            {
                int size = LocalRandom.randomInt(0, 5);
                var dict = new Dictionary<TKey, TValue>();

                for (int i = 0; i < size; i++)
                {
                    var key = keyGenerator();
                    if (!dict.ContainsKey(key))
                    {
                        dict.Add(key, valueGenerator());
                    }
                }
                return dict;
            };
        }


        public class BigMap<TKey, TValue>
        where TKey : notnull
        where TValue : notnull
        { }

        public class LiteralBigMap<TKey, TValue> : BigMap<TKey, TValue>
        where TKey : notnull
        where TValue : notnull
        {
            public Dictionary<TKey, TValue> _value;
            public LiteralBigMap(Dictionary<TKey, TValue> dict) { _value = dict; }
            public LiteralBigMap() { _value = new Dictionary<TKey, TValue>(); }
            public static implicit operator LiteralBigMap<TKey, TValue>(Dictionary<TKey, TValue> dict) => new LiteralBigMap<TKey, TValue>(dict);
        }

        public class AbstractBigMap<TKey, TValue> : BigMap<TKey, TValue>
        where TKey : notnull
        where TValue : notnull
        {
            private BigInteger _value;
            public AbstractBigMap(BigInteger n)
            {
                _value = n;
            }
            public static implicit operator BigInteger(AbstractBigMap<TKey, TValue> i) => i._value;
            public static implicit operator AbstractBigMap<TKey, TValue>(BigInteger i) => new AbstractBigMap<TKey, TValue>(i);
        }


        public static Func<BigMap<TKey, TValue>, IMicheline> BigMapEncode<TKey, TValue>(Func<TKey, IMicheline> keyEncode, Func<TValue, IMicheline> valueEncode) where TKey : notnull where TValue : notnull
        {
            return (BigMap<TKey, TValue> b) =>
            {
                switch (b)
                {
                    case AbstractBigMap<TKey, TValue> abm:
                        return DictEncode<TKey, TValue>(keyEncode, valueEncode)(new Dictionary<TKey, TValue>());
                    case LiteralBigMap<TKey, TValue> lbm:
                        return DictEncode<TKey, TValue>(keyEncode, valueEncode)(lbm._value);
                    default:
                        throw new EncodeError("Unknown case in big_mapEncode");
                };
            };
        }

        public static Func<IMicheline, BigMap<TKey, TValue>> BigMapDecode<TKey, TValue>(Func<IMicheline, TKey> keyDecode, Func<IMicheline, TValue> valueDecode) where TKey : notnull where TValue : notnull
        {
            return (IMicheline im) =>
            {
                switch (im)
                {
                    case MichelineInt mi:
                        return new AbstractBigMap<TKey, TValue>(mi.Value);
                    case MichelineArray ma:
                        var dict = new Dictionary<TKey, TValue>();
                        foreach (var item in ma)
                        {
                            dict.Append(DecodeMapElt<TKey, TValue>(keyDecode, valueDecode)(item));
                        }
                        return new LiteralBigMap<TKey, TValue>(dict);
                    default:
                        throw new DecodeError($"Could not decode bigmap from {im}");
                }
            };
        }

        public class Set<T> : List<T>
        {
            public Set() : base() { }
        }

        public class Contract
        {
            // TODO
        }

        public static IMicheline ContractEncode(Contract c)
        {
            throw new EncodeError("TODO: ContractEncode");
        }

        public static Contract ContractDecode(IMicheline m)
        {
            return new Contract();
        }

        public class Never
        {
            //TODO
        }

        public class Lambda
        {
            // TODO
        }


        public static IMicheline LambdaEncode(Lambda l)
        {
            throw new EncodeError("TODO: LambdaEncode");
        }

        public class Ticket
        {
            // TODO
        }

        public static IMicheline TicketEncode(Ticket t)
        {
            throw new EncodeError("TODO: TicketEncode");
        }

        public static Ticket TicketDecode(IMicheline m)
        {
            return new Ticket();
        }


        public class ChainId
        {
            private string _value;
            public ChainId(string addr)
            {
                _value = addr;
            }
            public static implicit operator string(ChainId a) => a._value;
            public static implicit operator ChainId(string s) => new ChainId(s);

        }

        public class Unit
        {
            public Unit() { }

        }

        public class Address
        {
            private string _value;
            public Address(string addr)
            {
                _value = addr;
            }
            public static implicit operator string(Address a) => a._value;
            public static implicit operator Address(string s) => new Address(s);
        }

        public class Signature
        {
            private string _value;
            public Signature(string addr)
            {
                _value = addr;
            }
            public static implicit operator string(Signature a) => a._value;
            public static implicit operator Signature(string s) => new Signature(s);
        }

        public class Timestamp
        {
            private string _value;
            public Timestamp(string b)
            {
                _value = b;
            }
            public static implicit operator string(Timestamp a) => a._value;
            public static implicit operator Timestamp(string b) => new Timestamp(b);
        }

        public class Key
        {
            private string _value;
            public Key(string addr)
            {
                _value = addr;
            }
            public static implicit operator string(Key a) => a._value;
            public static implicit operator Key(string s) => new Key(s);
        }

        public class KeyHash
        {
            private string _value;
            public KeyHash(string addr)
            {
                _value = addr;
            }
            public static implicit operator string(KeyHash a) => a._value;
            public static implicit operator KeyHash(string s) => new KeyHash(s);
        }

        public class Operation
        {
            private string _value;
            public Operation(string addr)
            {
                _value = addr;
            }
            public static implicit operator string(Operation a) => a._value;
            public static implicit operator Operation(string s) => new Operation(s);
        }

        public class Int
        {
            private BigInteger _value;
            public Int(BigInteger n)
            {
                _value = n;
            }
            public static implicit operator BigInteger(Int i) => i._value;
            public static implicit operator Int(BigInteger bi) => new Int(bi);
        }

        public class Nat
        {
            private BigInteger _value;
            public Nat(BigInteger n)
            {
                _value = n;
            }
            public static implicit operator BigInteger(Nat i) => i._value;
            public static implicit operator Nat(BigInteger bi) => new Nat(bi);
        }

        public class Tez
        {
            private BigInteger _value;
            public Tez(BigInteger n)
            {
                _value = n;
            }
            public static implicit operator BigInteger(Tez i) => i._value;
            public static implicit operator Tez(BigInteger bi) => new Tez(bi);
        }

        public static MichelineInt TezEncode(Tez t)
        {
            return new MichelineInt(t);
        }

        public static Tez TezDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelineInt mi:
                    return new Tez(mi.Value);
                default:
                    throw new DecodeError($"Couldn't decode tez value {im}");
            }
        }

        public class Bytes
        {
            private byte[] _bytes;

            public Bytes(byte[] b)
            {
                _bytes = b;
            }
            public static implicit operator Bytes(byte[] b) => new Bytes(b);
            public static implicit operator byte[](Bytes b) => b._bytes;


        }

        public class Bool
        {
            public bool b;

            public Bool(bool b)
            {
                this.b = b;
            }
            public static implicit operator Bool(bool b) => new Bool(b);
            public static implicit operator bool(Bool b) => b.b;
        }

        public static MichelineString SignatureEncode(Signature s)
        {
            return new MichelineString(s);
        }

        public static MichelineString ChainIdEncode(ChainId s)
        {
            return new MichelineString(s);
        }

        public static Signature SignatureDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelineString ms:
                    return new Signature(ms.Value);
                default:
                    throw new DecodeError($"Couldn't decode signature from {im}");
            }
        }

        public static ChainId ChainIdDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelineString ms:
                    return new ChainId(ms.Value);
                default:
                    throw new DecodeError($"Couldn't decode chain_id from {im}");
            }
        }


        public static MichelineString TimestampEncode(Timestamp s)
        {
            return new MichelineString(s);
        }

        public static Timestamp TimestampDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelineString ms:
                    return new Timestamp(ms.Value);
                default:
                    throw new DecodeError($"Couldn't decode timestamp from {im}");
            }
        }


        public static MichelineString KeyEncode(Key s)
        {
            return new MichelineString(s);
        }

        public static MichelineString KeyHashEncode(KeyHash s)
        {
            return new MichelineString(s);
        }

        public static Key KeyDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelineString ms:
                    return new Key(ms.Value);
                default:
                    throw new DecodeError($"Couldn't decode Key from {im}");
            }
        }

        public static KeyHash KeyHashDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelineString ms:
                    return new KeyHash(ms.Value);
                default:
                    throw new DecodeError($"Couldn't decode KeyHash from {im}");
            }
        }

        public static MichelineBytes BytesEncode(Bytes bytes)
        {
            return new MichelineBytes(bytes);
        }

        public static Bytes BytesDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelineBytes mb:
                    return mb.ToBytes();
                default:
                    throw new DecodeError($"Failed to decode bytes from {im}");
            }
        }

        public static MichelineInt IntEncode(Int i)
        {
            var res = new MichelineInt(i);
            return res;
        }

        public static Int IntDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelineInt mi:
                    var res = mi.Value;
                    return res;
                default:
                    throw new DecodeError($"A Micheline Int was expected, got {im}");
            }
        }

        public static MichelineInt NatEncode(Nat n)
        {
            var res = new MichelineInt(n);
            return res;
        }

        public static Nat NatDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelineInt mi:
                    var res = mi.Value;
                    return res;
                default:
                    throw new DecodeError($"A Micheline Int was expected, got {im}");
            }
        }

        public static MichelineString StringEncode(string s)
        {
            return new MichelineString(s);
        }

        public static string StringDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelineString ms:
                    var res = ms.Value;
                    return res;
                default:
                    throw new DecodeError($"A Micheline String was expected, got {im}");
            }
        }

        public static MichelineString AddressEncode(Address s)
        {
            return new MichelineString(s);
        }

        public static Address AddressDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelineString ms:
                    return new Address(ms.Value);
                default:
                    throw new DecodeError($"A Micheline String (Address) was expected, got {im}");
            }


        }

        public static MichelinePrim BoolEncode(Bool b)
        {
            if (b.b)
            {
                return new MichelinePrim { Prim = PrimType.True };
            }
            else
            {
                return new MichelinePrim { Prim = PrimType.True };
            }
        }

        public static Bool BoolDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelinePrim m:
                    switch (m.Prim)
                    {
                        case (PrimType.True):
                            return new Bool(true);
                        case (PrimType.False):
                            return new Bool(false);
                        default:
                            throw new DecodeError($"Couldn't decode boolean{m}");
                    }
                default:
                    throw new DecodeError($"Failed to decode a boolean out of {im}");
            }
        }

        public static MichelinePrim UnitEncode(Unit u)
        {
            return new MichelinePrim { Prim = PrimType.Unit };
        }

        public static Unit UnitDecode(IMicheline im)
        {
            switch (im)
            {
                case MichelinePrim mp:
                    switch (mp.Prim)
                    {
                        case PrimType.Unit:
                            return new Unit();
                        default:
                            throw new DecodeError($"Error decoding unit from {im}");
                    }
                default:
                    throw new DecodeError($"Error decoding unit from {im}");
            }
        }

        public class Option<T>
            where T : notnull
        {
        }

        public class Some<T> : Option<T> where T : notnull
        {
            public T? Value { get; set; }
            public Some(T value) { Value = value; }
        }

        public class None<T> : Option<T> where T : notnull
        {
            // Nothing
        }

        public static Func<Option<T>, MichelinePrim> OptionEncode<T>(Func<T, IMicheline> encode) where T : notnull
        {
            return (Option<T> v) =>
                {
                    switch (v)
                    {
                        case (Some<T> s):
                            if (s.Value == null)
                            {
                                throw new EncodeError($"null value in option {s}");
                            }
                            return new MichelinePrim
                            {
                                Prim = PrimType.Some,
                                Args = new List<IMicheline> { encode(s.Value) }
                            };
                        case (None<T> n):
                            return new MichelinePrim { Prim = PrimType.None };
                        default:
                            throw new EncodeError($"Error encoding option {v}");
                    }
                };

        }

        public static Func<IMicheline, Option<T>> OptionDecode<T>(Func<IMicheline, T> decode) where T : notnull
        {
            return (IMicheline im) =>
            {
                switch (im)
                {
                    case MichelinePrim mp:
                        switch (mp.Prim)
                        {
                            case PrimType.Some:
                                return new Some<T>(decode(mp.Args[0]));
                            case PrimType.None:
                                return new None<T>();
                            default:
                                throw new DecodeError($"Couldn't decode option value {im} looking at {mp}");
                        }
                    default:
                        throw new DecodeError($"Couldn't decode option value {im}");
                }
            };
        }

        public static Func<List<T>, MichelineArray> ListEncode<T>(Func<T, IMicheline> encode)
        {
            return (List<T> l) =>
            {
                var res = new MichelineArray();
                for (int i = 0; i < l.Count; i++)
                {
                    res.Add(encode(l[i]));
                }
                return res;
            };

        }

        public static Func<Set<T>, MichelineArray> SetEncode<T>(Func<T, IMicheline> encode)
        {
            return (Set<T> l) =>
            {
                var res = new MichelineArray();
                for (int i = 0; i < l.Count; i++)
                {
                    res.Add(encode(l[i]));
                }
                return res;
            };

        }

        public static MichelinePrim make_right(IMicheline v)
        {
            var res = new MichelinePrim
            {
                Prim = PrimType.Right,
                Args = new List<IMicheline> { v }
            };
            return res;
        }

        public static MichelinePrim make_left(IMicheline v)
        {
            var res = new MichelinePrim
            {
                Prim = PrimType.Left,
                Args = new List<IMicheline> { v }
            };
            return res;
        }


        public static Func<MyTuple<T1, T2>, IMicheline> Tuple2Encode<T1, T2>(Func<T1, IMicheline> t1Encode, Func<T2, IMicheline> t2Encode)
        {
            return (MyTuple<T1, T2> arg) =>
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
                        t1Encode(arg.Item1),
                        t2Encode(arg.Item2)
                    }

                };
            };
        }

        public static Func<IMicheline, MyTuple<T1, T2>> Tuple2Decode<T1, T2>(Func<IMicheline, T1> t1Decode, Func<IMicheline, T2> t2Decode)
        {
            return (IMicheline im) =>
            {
                var error = new DecodeError($"In Tuple2Decode, failed on {im}");
                switch (im)
                {
                    case (MichelinePrim mp):
                        switch (mp.Prim)
                        {
                            case (PrimType.Pair):
                                if (mp.Args.Count < 2)
                                {
                                    throw new DecodeError($"In Tuple2Decode, there are less than two elements in {mp}");
                                }
                                else
                                {
                                    T1 first = t1Decode(mp.Args[0]);
                                    T2 second = t2Decode(mp.Args[1]);
                                    return new MyTuple<T1, T2>(first, second);
                                }
                            default:
                                throw error;
                        }
                    default:
                        throw error;
                }
            };
        }




        public static Func<MyTuple<T1, T2, T3>, IMicheline> Tuple3Encode<T1, T2, T3>(Func<T1, IMicheline> t1Encode, Func<T2, IMicheline> t2Encode, Func<T3, IMicheline> t3Encode)
        {
            return (MyTuple<T1, T2, T3> arg) =>
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
                        t1Encode(arg.Item1), t2Encode(arg.Item2), t3Encode(arg.Item3)
                    }

                };
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4>, IMicheline> Tuple4Encode<T1, T2, T3, T4>(Func<T1, IMicheline> t1Encode, Func<T2, IMicheline> t2Encode, Func<T3, IMicheline> t3Encode, Func<T4, IMicheline> t4Encode)
        {
            return (MyTuple<T1, T2, T3, T4> arg) =>
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
                        t1Encode(arg.Item1), t2Encode(arg.Item2), t3Encode(arg.Item3), t4Encode(arg.Item4)
                    }

                };
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5>, IMicheline> Tuple5Encode<T1, T2, T3, T4, T5>(Func<T1, IMicheline> t1Encode, Func<T2, IMicheline> t2Encode, Func<T3, IMicheline> t3Encode, Func<T4, IMicheline> t4Encode, Func<T5, IMicheline> t5Encode)
        {
            return (MyTuple<T1, T2, T3, T4, T5> arg) =>
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
                        t1Encode(arg.Item1), t2Encode(arg.Item2), t3Encode(arg.Item3), t4Encode(arg.Item4), t5Encode(arg.Item5)
                    }

                };
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5, T6>, IMicheline> Tuple6Encode<T1, T2, T3, T4, T5, T6>(Func<T1, IMicheline> t1Encode, Func<T2, IMicheline> t2Encode, Func<T3, IMicheline> t3Encode, Func<T4, IMicheline> t4Encode, Func<T5, IMicheline> t5Encode, Func<T6, IMicheline> t6Encode)
        {
            return (MyTuple<T1, T2, T3, T4, T5, T6> arg) =>
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
                        t1Encode(arg.Item1), t2Encode(arg.Item2), t3Encode(arg.Item3), t4Encode(arg.Item4), t5Encode(arg.Item5), t6Encode(arg.Item6)
                    }

                };
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5, T6, T7>, IMicheline> Tuple7Encode<T1, T2, T3, T4, T5, T6, T7>(Func<T1, IMicheline> t1Encode, Func<T2, IMicheline> t2Encode, Func<T3, IMicheline> t3Encode, Func<T4, IMicheline> t4Encode, Func<T5, IMicheline> t5Encode, Func<T6, IMicheline> t6Encode, Func<T7, IMicheline> t7Encode)
        {
            return (MyTuple<T1, T2, T3, T4, T5, T6, T7> arg) =>
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
                        t1Encode(arg.Item1), t2Encode(arg.Item2), t3Encode(arg.Item3), t4Encode(arg.Item4), t5Encode(arg.Item5), t6Encode(arg.Item6), t7Encode(arg.Item7)
                    }

                };
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5, T6, T7, T8>, IMicheline> Tuple8Encode<T1, T2, T3, T4, T5, T6, T7, T8>(Func<T1, IMicheline> t1Encode, Func<T2, IMicheline> t2Encode, Func<T3, IMicheline> t3Encode, Func<T4, IMicheline> t4Encode, Func<T5, IMicheline> t5Encode, Func<T6, IMicheline> t6Encode, Func<T7, IMicheline> t7Encode, Func<T8, IMicheline> t8Encode)
        {
            return (MyTuple<T1, T2, T3, T4, T5, T6, T7, T8> arg) =>
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
                        t1Encode(arg.Item1), t2Encode(arg.Item2), t3Encode(arg.Item3), t4Encode(arg.Item4), t5Encode(arg.Item5), t6Encode(arg.Item6), t7Encode(arg.Item7), t8Encode(arg.Rest.Item1)
                    }

                };
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>, IMicheline> Tuple9Encode<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Func<T1, IMicheline> t1Encode, Func<T2, IMicheline> t2Encode, Func<T3, IMicheline> t3Encode, Func<T4, IMicheline> t4Encode, Func<T5, IMicheline> t5Encode, Func<T6, IMicheline> t6Encode, Func<T7, IMicheline> t7Encode, Func<T8, IMicheline> t8Encode, Func<T9, IMicheline> t9Encode)
        {
            return (MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9> arg) =>
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
                        t1Encode(arg.Item1), t2Encode(arg.Item2), t3Encode(arg.Item3), t4Encode(arg.Item4), t5Encode(arg.Item5), t6Encode(arg.Item6), t7Encode(arg.Item7), t8Encode(arg.Rest.Item1), t9Encode(arg.Rest.Item2)
                    }

                };
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>, IMicheline> Tuple10Encode<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Func<T1, IMicheline> t1Encode, Func<T2, IMicheline> t2Encode, Func<T3, IMicheline> t3Encode, Func<T4, IMicheline> t4Encode, Func<T5, IMicheline> t5Encode, Func<T6, IMicheline> t6Encode, Func<T7, IMicheline> t7Encode, Func<T8, IMicheline> t8Encode, Func<T9, IMicheline> t9Encode, Func<T10, IMicheline> t10Encode)
        {
            return (MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> arg) =>
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
                        t1Encode(arg.Item1), t2Encode(arg.Item2), t3Encode(arg.Item3), t4Encode(arg.Item4), t5Encode(arg.Item5), t6Encode(arg.Item6), t7Encode(arg.Item7), t8Encode(arg.Rest.Item1), t9Encode(arg.Rest.Item2), t10Encode(arg.Rest.Item3)
                    }

                };
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>, IMicheline> Tuple11Encode<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Func<T1, IMicheline> t1Encode, Func<T2, IMicheline> t2Encode, Func<T3, IMicheline> t3Encode, Func<T4, IMicheline> t4Encode, Func<T5, IMicheline> t5Encode, Func<T6, IMicheline> t6Encode, Func<T7, IMicheline> t7Encode, Func<T8, IMicheline> t8Encode, Func<T9, IMicheline> t9Encode, Func<T10, IMicheline> t10Encode, Func<T11, IMicheline> t11Encode)
        {
            return (MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> arg) =>
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
                        t1Encode(arg.Item1), t2Encode(arg.Item2), t3Encode(arg.Item3), t4Encode(arg.Item4), t5Encode(arg.Item5), t6Encode(arg.Item6), t7Encode(arg.Item7), t8Encode(arg.Rest.Item1), t9Encode(arg.Rest.Item2), t10Encode(arg.Rest.Item3), t11Encode(arg.Rest.Item4)
                    }

                };
            };
        }



        public static Func<T1, MyTuple<T1, T2>> Tuple1_to_Tuple2<T1, T2>(T2 x2) { return (T1 x) => { return new MyTuple<T1, T2>(x, x2); }; }

        public static Func<T1, MyTuple<T1, T2, T3>> Tuple2_to_Tuple3<T1, T2, T3>(T2 x2, T3 x3) { return (T1 x) => { return new MyTuple<T1, T2, T3>(x, x2, x3); }; }

        public static Func<T1, MyTuple<T1, T2, T3, T4>> Tuple3_to_Tuple4<T1, T2, T3, T4>(T2 x2, T3 x3, T4 x4) { return (T1 x) => { return new MyTuple<T1, T2, T3, T4>(x, x2, x3, x4); }; }

        public static Func<T1, MyTuple<T1, T2, T3, T4, T5>> Tuple4_to_Tuple5<T1, T2, T3, T4, T5>(T2 x2, T3 x3, T4 x4, T5 x5) { return (T1 x) => { return new MyTuple<T1, T2, T3, T4, T5>(x, x2, x3, x4, x5); }; }

        public static Func<T1, MyTuple<T1, T2, T3, T4, T5, T6>> Tuple5_to_Tuple6<T1, T2, T3, T4, T5, T6>(T2 x2, T3 x3, T4 x4, T5 x5, T6 x6) { return (T1 x) => { return new MyTuple<T1, T2, T3, T4, T5, T6>(x, x2, x3, x4, x5, x6); }; }

        public static Func<T1, MyTuple<T1, T2, T3, T4, T5, T6, T7>> Tuple6_to_Tuple7<T1, T2, T3, T4, T5, T6, T7>(T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7) { return (T1 x) => { return new MyTuple<T1, T2, T3, T4, T5, T6, T7>(x, x2, x3, x4, x5, x6, x7); }; }

        public static Func<T1, MyTuple<T1, T2, T3, T4, T5, T6, T7, T8>> Tuple7_to_Tuple8<T1, T2, T3, T4, T5, T6, T7, T8>(T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8) { return (T1 x) => { return new MyTuple<T1, T2, T3, T4, T5, T6, T7, T8>(x, x2, x3, x4, x5, x6, x7, x8); }; }

        public static Func<T1, MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>> Tuple8_to_Tuple9<T1, T2, T3, T4, T5, T6, T7, T8, T9>(T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9) { return (T1 x) => { return new MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>(x, x2, x3, x4, x5, x6, x7, x8, x9); }; }

        public static Func<T1, MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>> Tuple9_to_Tuple10<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10) { return (T1 x) => { return new MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(x, x2, x3, x4, x5, x6, x7, x8, x9, x10); }; }

        public static Func<T1, MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>> Tuple10_to_Tuple11<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10, T11 x11) { return (T1 x) => { return new MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(x, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11); }; }



        public static Func<IMicheline, MyTuple<T1, T2, T3>> Tuple3Decode<T1, T2, T3>(Func<IMicheline, T1> t1Decode, Func<IMicheline, T2> t2Decode, Func<IMicheline, T3> t3Decode)
        {
            return (IMicheline im) =>
            {
                var error = new DecodeError($"In Tuple3Decode, failed on {im}");
                List<IMicheline> args;
                switch (im)
                {
                    case MichelinePrim mp:
                        switch (mp.Prim)
                        {
                            case PrimType.Pair:
                                args = mp.Args;
                                break;
                            default:
                                throw new DecodeError($"This is a prim but not a pair, can't be in TupleDecode {mp}");
                        }
                        break;
                    case MichelineArray ma:
                        args = ma;
                        break;
                    default:
                        throw new DecodeError($"This is neither a Pair nor a Micheline Array, can't decode");
                }
                if (args.Count < 2)
                {
                    throw new DecodeError($"In Tuple3Decode, there are less than two elements in {args}");
                }
                else
                {
                    if (args.Count > 2)
                    {
                        T1 first = t1Decode(args[0]);
                        List<IMicheline> rest = new List<IMicheline>(args.Skip(1).ToArray());
                        MichelineArray ma = new MichelineArray();
                        foreach (var x in rest)
                        {
                            ma.Append(x);
                        }
                        MyTuple<T2, T3> second = Tuple2Decode<T2, T3>(t2Decode, t3Decode)(ma);
                        return Tuple2_to_Tuple3<T1, T2, T3>(second.Item1, second.Item2)(first);
                    }
                    else
                    {//there are at exactly two elements
                        T1 first = t1Decode(args[0]);
                        MichelineArray ma = new MichelineArray();
                        ma.Append(args[1]);
                        MyTuple<T2, T3> second = Tuple2Decode<T2, T3>(t2Decode, t3Decode)(ma);
                        return Tuple2_to_Tuple3<T1, T2, T3>(second.Item1, second.Item2)(first);
                    }
                }
            };
        }




        public static Func<IMicheline, MyTuple<T1, T2, T3, T4>> Tuple4Decode<T1, T2, T3, T4>(Func<IMicheline, T1> t1Decode, Func<IMicheline, T2> t2Decode, Func<IMicheline, T3> t3Decode, Func<IMicheline, T4> t4Decode)
        {
            return (IMicheline im) =>
            {
                var error = new DecodeError($"In Tuple4Decode, failed on {im}");
                List<IMicheline> args;
                switch (im)
                {
                    case MichelinePrim mp:
                        switch (mp.Prim)
                        {
                            case PrimType.Pair:
                                args = mp.Args;
                                break;
                            default:
                                throw new DecodeError($"This is a prim but not a pair, can't be in TupleDecode {mp}");
                        }
                        break;
                    case MichelineArray ma:
                        args = ma;
                        break;
                    default:
                        throw new DecodeError($"This is neither a Pair nor a Micheline Array, can't decode");
                }
                if (args.Count < 2)
                {
                    throw new DecodeError($"In Tuple4Decode, there are less than two elements in {args}");
                }
                else
                {
                    if (args.Count > 2)
                    {
                        T1 first = t1Decode(args[0]);
                        List<IMicheline> rest = new List<IMicheline>(args.Skip(1).ToArray());
                        MichelineArray ma = new MichelineArray();
                        foreach (var x in rest)
                        {
                            ma.Append(x);
                        }
                        MyTuple<T2, T3, T4> second = Tuple3Decode<T2, T3, T4>(t2Decode, t3Decode, t4Decode)(ma);
                        return Tuple3_to_Tuple4<T1, T2, T3, T4>(second.Item1, second.Item2, second.Item3)(first);
                    }
                    else
                    {//there are at exactly two elements
                        T1 first = t1Decode(args[0]);
                        MichelineArray ma = new MichelineArray();
                        ma.Append(args[1]);
                        MyTuple<T2, T3, T4> second = Tuple3Decode<T2, T3, T4>(t2Decode, t3Decode, t4Decode)(ma);
                        return Tuple3_to_Tuple4<T1, T2, T3, T4>(second.Item1, second.Item2, second.Item3)(first);
                    }
                }
            };
        }




        public static Func<IMicheline, MyTuple<T1, T2, T3, T4, T5>> Tuple5Decode<T1, T2, T3, T4, T5>(Func<IMicheline, T1> t1Decode, Func<IMicheline, T2> t2Decode, Func<IMicheline, T3> t3Decode, Func<IMicheline, T4> t4Decode, Func<IMicheline, T5> t5Decode)
        {
            return (IMicheline im) =>
            {
                var error = new DecodeError($"In Tuple5Decode, failed on {im}");
                List<IMicheline> args;
                switch (im)
                {
                    case MichelinePrim mp:
                        switch (mp.Prim)
                        {
                            case PrimType.Pair:
                                args = mp.Args;
                                break;
                            default:
                                throw new DecodeError($"This is a prim but not a pair, can't be in TupleDecode {mp}");
                        }
                        break;
                    case MichelineArray ma:
                        args = ma;
                        break;
                    default:
                        throw new DecodeError($"This is neither a Pair nor a Micheline Array, can't decode");
                }
                if (args.Count < 2)
                {
                    throw new DecodeError($"In Tuple5Decode, there are less than two elements in {args}");
                }
                else
                {
                    if (args.Count > 2)
                    {
                        T1 first = t1Decode(args[0]);
                        List<IMicheline> rest = new List<IMicheline>(args.Skip(1).ToArray());
                        MichelineArray ma = new MichelineArray();
                        foreach (var x in rest)
                        {
                            ma.Append(x);
                        }
                        MyTuple<T2, T3, T4, T5> second = Tuple4Decode<T2, T3, T4, T5>(t2Decode, t3Decode, t4Decode, t5Decode)(ma);
                        return Tuple4_to_Tuple5<T1, T2, T3, T4, T5>(second.Item1, second.Item2, second.Item3, second.Item4)(first);
                    }
                    else
                    {//there are at exactly two elements
                        T1 first = t1Decode(args[0]);
                        MichelineArray ma = new MichelineArray();
                        ma.Append(args[1]);
                        MyTuple<T2, T3, T4, T5> second = Tuple4Decode<T2, T3, T4, T5>(t2Decode, t3Decode, t4Decode, t5Decode)(ma);
                        return Tuple4_to_Tuple5<T1, T2, T3, T4, T5>(second.Item1, second.Item2, second.Item3, second.Item4)(first);
                    }
                }
            };
        }




        public static Func<IMicheline, MyTuple<T1, T2, T3, T4, T5, T6>> Tuple6Decode<T1, T2, T3, T4, T5, T6>(Func<IMicheline, T1> t1Decode, Func<IMicheline, T2> t2Decode, Func<IMicheline, T3> t3Decode, Func<IMicheline, T4> t4Decode, Func<IMicheline, T5> t5Decode, Func<IMicheline, T6> t6Decode)
        {
            return (IMicheline im) =>
            {
                var error = new DecodeError($"In Tuple6Decode, failed on {im}");
                List<IMicheline> args;
                switch (im)
                {
                    case MichelinePrim mp:
                        switch (mp.Prim)
                        {
                            case PrimType.Pair:
                                args = mp.Args;
                                break;
                            default:
                                throw new DecodeError($"This is a prim but not a pair, can't be in TupleDecode {mp}");
                        }
                        break;
                    case MichelineArray ma:
                        args = ma;
                        break;
                    default:
                        throw new DecodeError($"This is neither a Pair nor a Micheline Array, can't decode");
                }
                if (args.Count < 2)
                {
                    throw new DecodeError($"In Tuple6Decode, there are less than two elements in {args}");
                }
                else
                {
                    if (args.Count > 2)
                    {
                        T1 first = t1Decode(args[0]);
                        List<IMicheline> rest = new List<IMicheline>(args.Skip(1).ToArray());
                        MichelineArray ma = new MichelineArray();
                        foreach (var x in rest)
                        {
                            ma.Append(x);
                        }
                        MyTuple<T2, T3, T4, T5, T6> second = Tuple5Decode<T2, T3, T4, T5, T6>(t2Decode, t3Decode, t4Decode, t5Decode, t6Decode)(ma);
                        return Tuple5_to_Tuple6<T1, T2, T3, T4, T5, T6>(second.Item1, second.Item2, second.Item3, second.Item4, second.Item5)(first);
                    }
                    else
                    {//there are at exactly two elements
                        T1 first = t1Decode(args[0]);
                        MichelineArray ma = new MichelineArray();
                        ma.Append(args[1]);
                        MyTuple<T2, T3, T4, T5, T6> second = Tuple5Decode<T2, T3, T4, T5, T6>(t2Decode, t3Decode, t4Decode, t5Decode, t6Decode)(ma);
                        return Tuple5_to_Tuple6<T1, T2, T3, T4, T5, T6>(second.Item1, second.Item2, second.Item3, second.Item4, second.Item5)(first);
                    }
                }
            };
        }




        public static Func<IMicheline, MyTuple<T1, T2, T3, T4, T5, T6, T7>> Tuple7Decode<T1, T2, T3, T4, T5, T6, T7>(Func<IMicheline, T1> t1Decode, Func<IMicheline, T2> t2Decode, Func<IMicheline, T3> t3Decode, Func<IMicheline, T4> t4Decode, Func<IMicheline, T5> t5Decode, Func<IMicheline, T6> t6Decode, Func<IMicheline, T7> t7Decode)
        {
            return (IMicheline im) =>
            {
                var error = new DecodeError($"In Tuple7Decode, failed on {im}");
                List<IMicheline> args;
                switch (im)
                {
                    case MichelinePrim mp:
                        switch (mp.Prim)
                        {
                            case PrimType.Pair:
                                args = mp.Args;
                                break;
                            default:
                                throw new DecodeError($"This is a prim but not a pair, can't be in TupleDecode {mp}");
                        }
                        break;
                    case MichelineArray ma:
                        args = ma;
                        break;
                    default:
                        throw new DecodeError($"This is neither a Pair nor a Micheline Array, can't decode");
                }
                if (args.Count < 2)
                {
                    throw new DecodeError($"In Tuple7Decode, there are less than two elements in {args}");
                }
                else
                {
                    if (args.Count > 2)
                    {
                        T1 first = t1Decode(args[0]);
                        List<IMicheline> rest = new List<IMicheline>(args.Skip(1).ToArray());
                        MichelineArray ma = new MichelineArray();
                        foreach (var x in rest)
                        {
                            ma.Append(x);
                        }
                        MyTuple<T2, T3, T4, T5, T6, T7> second = Tuple6Decode<T2, T3, T4, T5, T6, T7>(t2Decode, t3Decode, t4Decode, t5Decode, t6Decode, t7Decode)(ma);
                        return Tuple6_to_Tuple7<T1, T2, T3, T4, T5, T6, T7>(second.Item1, second.Item2, second.Item3, second.Item4, second.Item5, second.Item6)(first);
                    }
                    else
                    {//there are at exactly two elements
                        T1 first = t1Decode(args[0]);
                        MichelineArray ma = new MichelineArray();
                        ma.Append(args[1]);
                        MyTuple<T2, T3, T4, T5, T6, T7> second = Tuple6Decode<T2, T3, T4, T5, T6, T7>(t2Decode, t3Decode, t4Decode, t5Decode, t6Decode, t7Decode)(ma);
                        return Tuple6_to_Tuple7<T1, T2, T3, T4, T5, T6, T7>(second.Item1, second.Item2, second.Item3, second.Item4, second.Item5, second.Item6)(first);
                    }
                }
            };
        }




        public static Func<IMicheline, MyTuple<T1, T2, T3, T4, T5, T6, T7, T8>> Tuple8Decode<T1, T2, T3, T4, T5, T6, T7, T8>(Func<IMicheline, T1> t1Decode, Func<IMicheline, T2> t2Decode, Func<IMicheline, T3> t3Decode, Func<IMicheline, T4> t4Decode, Func<IMicheline, T5> t5Decode, Func<IMicheline, T6> t6Decode, Func<IMicheline, T7> t7Decode, Func<IMicheline, T8> t8Decode)
        {
            return (IMicheline im) =>
            {
                var error = new DecodeError($"In Tuple8Decode, failed on {im}");
                List<IMicheline> args;
                switch (im)
                {
                    case MichelinePrim mp:
                        switch (mp.Prim)
                        {
                            case PrimType.Pair:
                                args = mp.Args;
                                break;
                            default:
                                throw new DecodeError($"This is a prim but not a pair, can't be in TupleDecode {mp}");
                        }
                        break;
                    case MichelineArray ma:
                        args = ma;
                        break;
                    default:
                        throw new DecodeError($"This is neither a Pair nor a Micheline Array, can't decode");
                }
                if (args.Count < 2)
                {
                    throw new DecodeError($"In Tuple8Decode, there are less than two elements in {args}");
                }
                else
                {
                    if (args.Count > 2)
                    {
                        T1 first = t1Decode(args[0]);
                        List<IMicheline> rest = new List<IMicheline>(args.Skip(1).ToArray());
                        MichelineArray ma = new MichelineArray();
                        foreach (var x in rest)
                        {
                            ma.Append(x);
                        }
                        MyTuple<T2, T3, T4, T5, T6, T7, T8> second = Tuple7Decode<T2, T3, T4, T5, T6, T7, T8>(t2Decode, t3Decode, t4Decode, t5Decode, t6Decode, t7Decode, t8Decode)(ma);
                        return Tuple7_to_Tuple8<T1, T2, T3, T4, T5, T6, T7, T8>(second.Item1, second.Item2, second.Item3, second.Item4, second.Item5, second.Item6, second.Item7)(first);
                    }
                    else
                    {//there are at exactly two elements
                        T1 first = t1Decode(args[0]);
                        MichelineArray ma = new MichelineArray();
                        ma.Append(args[1]);
                        MyTuple<T2, T3, T4, T5, T6, T7, T8> second = Tuple7Decode<T2, T3, T4, T5, T6, T7, T8>(t2Decode, t3Decode, t4Decode, t5Decode, t6Decode, t7Decode, t8Decode)(ma);
                        return Tuple7_to_Tuple8<T1, T2, T3, T4, T5, T6, T7, T8>(second.Item1, second.Item2, second.Item3, second.Item4, second.Item5, second.Item6, second.Item7)(first);
                    }
                }
            };
        }




        public static Func<IMicheline, MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>> Tuple9Decode<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Func<IMicheline, T1> t1Decode, Func<IMicheline, T2> t2Decode, Func<IMicheline, T3> t3Decode, Func<IMicheline, T4> t4Decode, Func<IMicheline, T5> t5Decode, Func<IMicheline, T6> t6Decode, Func<IMicheline, T7> t7Decode, Func<IMicheline, T8> t8Decode, Func<IMicheline, T9> t9Decode)
        {
            return (IMicheline im) =>
            {
                var error = new DecodeError($"In Tuple9Decode, failed on {im}");
                List<IMicheline> args;
                switch (im)
                {
                    case MichelinePrim mp:
                        switch (mp.Prim)
                        {
                            case PrimType.Pair:
                                args = mp.Args;
                                break;
                            default:
                                throw new DecodeError($"This is a prim but not a pair, can't be in TupleDecode {mp}");
                        }
                        break;
                    case MichelineArray ma:
                        args = ma;
                        break;
                    default:
                        throw new DecodeError($"This is neither a Pair nor a Micheline Array, can't decode");
                }
                if (args.Count < 2)
                {
                    throw new DecodeError($"In Tuple9Decode, there are less than two elements in {args}");
                }
                else
                {
                    if (args.Count > 2)
                    {
                        T1 first = t1Decode(args[0]);
                        List<IMicheline> rest = new List<IMicheline>(args.Skip(1).ToArray());
                        MichelineArray ma = new MichelineArray();
                        foreach (var x in rest)
                        {
                            ma.Append(x);
                        }
                        MyTuple<T2, T3, T4, T5, T6, T7, T8, T9> second = Tuple8Decode<T2, T3, T4, T5, T6, T7, T8, T9>(t2Decode, t3Decode, t4Decode, t5Decode, t6Decode, t7Decode, t8Decode, t9Decode)(ma);
                        return Tuple8_to_Tuple9<T1, T2, T3, T4, T5, T6, T7, T8, T9>(second.Item1, second.Item2, second.Item3, second.Item4, second.Item5, second.Item6, second.Item7, second.Rest.Item1)(first);
                    }
                    else
                    {//there are at exactly two elements
                        T1 first = t1Decode(args[0]);
                        MichelineArray ma = new MichelineArray();
                        ma.Append(args[1]);
                        MyTuple<T2, T3, T4, T5, T6, T7, T8, T9> second = Tuple8Decode<T2, T3, T4, T5, T6, T7, T8, T9>(t2Decode, t3Decode, t4Decode, t5Decode, t6Decode, t7Decode, t8Decode, t9Decode)(ma);
                        return Tuple8_to_Tuple9<T1, T2, T3, T4, T5, T6, T7, T8, T9>(second.Item1, second.Item2, second.Item3, second.Item4, second.Item5, second.Item6, second.Item7, second.Rest.Item1)(first);
                    }
                }
            };
        }




        public static Func<IMicheline, MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>> Tuple10Decode<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Func<IMicheline, T1> t1Decode, Func<IMicheline, T2> t2Decode, Func<IMicheline, T3> t3Decode, Func<IMicheline, T4> t4Decode, Func<IMicheline, T5> t5Decode, Func<IMicheline, T6> t6Decode, Func<IMicheline, T7> t7Decode, Func<IMicheline, T8> t8Decode, Func<IMicheline, T9> t9Decode, Func<IMicheline, T10> t10Decode)
        {
            return (IMicheline im) =>
            {
                var error = new DecodeError($"In Tuple10Decode, failed on {im}");
                List<IMicheline> args;
                switch (im)
                {
                    case MichelinePrim mp:
                        switch (mp.Prim)
                        {
                            case PrimType.Pair:
                                args = mp.Args;
                                break;
                            default:
                                throw new DecodeError($"This is a prim but not a pair, can't be in TupleDecode {mp}");
                        }
                        break;
                    case MichelineArray ma:
                        args = ma;
                        break;
                    default:
                        throw new DecodeError($"This is neither a Pair nor a Micheline Array, can't decode");
                }
                if (args.Count < 2)
                {
                    throw new DecodeError($"In Tuple10Decode, there are less than two elements in {args}");
                }
                else
                {
                    if (args.Count > 2)
                    {
                        T1 first = t1Decode(args[0]);
                        List<IMicheline> rest = new List<IMicheline>(args.Skip(1).ToArray());
                        MichelineArray ma = new MichelineArray();
                        foreach (var x in rest)
                        {
                            ma.Append(x);
                        }
                        MyTuple<T2, T3, T4, T5, T6, T7, T8, T9, T10> second = Tuple9Decode<T2, T3, T4, T5, T6, T7, T8, T9, T10>(t2Decode, t3Decode, t4Decode, t5Decode, t6Decode, t7Decode, t8Decode, t9Decode, t10Decode)(ma);
                        return Tuple9_to_Tuple10<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(second.Item1, second.Item2, second.Item3, second.Item4, second.Item5, second.Item6, second.Item7, second.Rest.Item1, second.Rest.Item2)(first);
                    }
                    else
                    {//there are at exactly two elements
                        T1 first = t1Decode(args[0]);
                        MichelineArray ma = new MichelineArray();
                        ma.Append(args[1]);
                        MyTuple<T2, T3, T4, T5, T6, T7, T8, T9, T10> second = Tuple9Decode<T2, T3, T4, T5, T6, T7, T8, T9, T10>(t2Decode, t3Decode, t4Decode, t5Decode, t6Decode, t7Decode, t8Decode, t9Decode, t10Decode)(ma);
                        return Tuple9_to_Tuple10<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(second.Item1, second.Item2, second.Item3, second.Item4, second.Item5, second.Item6, second.Item7, second.Rest.Item1, second.Rest.Item2)(first);
                    }
                }
            };
        }




        public static Func<IMicheline, MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>> Tuple11Decode<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Func<IMicheline, T1> t1Decode, Func<IMicheline, T2> t2Decode, Func<IMicheline, T3> t3Decode, Func<IMicheline, T4> t4Decode, Func<IMicheline, T5> t5Decode, Func<IMicheline, T6> t6Decode, Func<IMicheline, T7> t7Decode, Func<IMicheline, T8> t8Decode, Func<IMicheline, T9> t9Decode, Func<IMicheline, T10> t10Decode, Func<IMicheline, T11> t11Decode)
        {
            return (IMicheline im) =>
            {
                var error = new DecodeError($"In Tuple11Decode, failed on {im}");
                List<IMicheline> args;
                switch (im)
                {
                    case MichelinePrim mp:
                        switch (mp.Prim)
                        {
                            case PrimType.Pair:
                                args = mp.Args;
                                break;
                            default:
                                throw new DecodeError($"This is a prim but not a pair, can't be in TupleDecode {mp}");
                        }
                        break;
                    case MichelineArray ma:
                        args = ma;
                        break;
                    default:
                        throw new DecodeError($"This is neither a Pair nor a Micheline Array, can't decode");
                }
                if (args.Count < 2)
                {
                    throw new DecodeError($"In Tuple11Decode, there are less than two elements in {args}");
                }
                else
                {
                    if (args.Count > 2)
                    {
                        T1 first = t1Decode(args[0]);
                        List<IMicheline> rest = new List<IMicheline>(args.Skip(1).ToArray());
                        MichelineArray ma = new MichelineArray();
                        foreach (var x in rest)
                        {
                            ma.Append(x);
                        }
                        MyTuple<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> second = Tuple10Decode<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(t2Decode, t3Decode, t4Decode, t5Decode, t6Decode, t7Decode, t8Decode, t9Decode, t10Decode, t11Decode)(ma);
                        return Tuple10_to_Tuple11<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(second.Item1, second.Item2, second.Item3, second.Item4, second.Item5, second.Item6, second.Item7, second.Rest.Item1, second.Rest.Item2, second.Rest.Item3)(first);
                    }
                    else
                    {//there are at exactly two elements
                        T1 first = t1Decode(args[0]);
                        MichelineArray ma = new MichelineArray();
                        ma.Append(args[1]);
                        MyTuple<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> second = Tuple10Decode<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(t2Decode, t3Decode, t4Decode, t5Decode, t6Decode, t7Decode, t8Decode, t9Decode, t10Decode, t11Decode)(ma);
                        return Tuple10_to_Tuple11<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(second.Item1, second.Item2, second.Item3, second.Item4, second.Item5, second.Item6, second.Item7, second.Rest.Item1, second.Rest.Item2, second.Rest.Item3)(first);
                    }
                }
            };
        }





        public static Func<MyTuple<T1, T2>> Tuple2Generator<T1, T2>(Func<T1> t1Generator, Func<T2> t2Generator)
        {
            return () =>
            {
                return new MyTuple<T1, T2>(t1Generator(), t2Generator());
            };
        }




        public static Func<MyTuple<T1, T2, T3>> Tuple3Generator<T1, T2, T3>(Func<T1> t1Generator, Func<T2> t2Generator, Func<T3> t3Generator)
        {
            return () =>
            {
                return new MyTuple<T1, T2, T3>(t1Generator(), t2Generator(), t3Generator());
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4>> Tuple4Generator<T1, T2, T3, T4>(Func<T1> t1Generator, Func<T2> t2Generator, Func<T3> t3Generator, Func<T4> t4Generator)
        {
            return () =>
            {
                return new MyTuple<T1, T2, T3, T4>(t1Generator(), t2Generator(), t3Generator(), t4Generator());
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5>> Tuple5Generator<T1, T2, T3, T4, T5>(Func<T1> t1Generator, Func<T2> t2Generator, Func<T3> t3Generator, Func<T4> t4Generator, Func<T5> t5Generator)
        {
            return () =>
            {
                return new MyTuple<T1, T2, T3, T4, T5>(t1Generator(), t2Generator(), t3Generator(), t4Generator(), t5Generator());
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5, T6>> Tuple6Generator<T1, T2, T3, T4, T5, T6>(Func<T1> t1Generator, Func<T2> t2Generator, Func<T3> t3Generator, Func<T4> t4Generator, Func<T5> t5Generator, Func<T6> t6Generator)
        {
            return () =>
            {
                return new MyTuple<T1, T2, T3, T4, T5, T6>(t1Generator(), t2Generator(), t3Generator(), t4Generator(), t5Generator(), t6Generator());
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5, T6, T7>> Tuple7Generator<T1, T2, T3, T4, T5, T6, T7>(Func<T1> t1Generator, Func<T2> t2Generator, Func<T3> t3Generator, Func<T4> t4Generator, Func<T5> t5Generator, Func<T6> t6Generator, Func<T7> t7Generator)
        {
            return () =>
            {
                return new MyTuple<T1, T2, T3, T4, T5, T6, T7>(t1Generator(), t2Generator(), t3Generator(), t4Generator(), t5Generator(), t6Generator(), t7Generator());
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5, T6, T7, T8>> Tuple8Generator<T1, T2, T3, T4, T5, T6, T7, T8>(Func<T1> t1Generator, Func<T2> t2Generator, Func<T3> t3Generator, Func<T4> t4Generator, Func<T5> t5Generator, Func<T6> t6Generator, Func<T7> t7Generator, Func<T8> t8Generator)
        {
            return () =>
            {
                return new MyTuple<T1, T2, T3, T4, T5, T6, T7, T8>(t1Generator(), t2Generator(), t3Generator(), t4Generator(), t5Generator(), t6Generator(), t7Generator(), t8Generator());
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>> Tuple9Generator<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Func<T1> t1Generator, Func<T2> t2Generator, Func<T3> t3Generator, Func<T4> t4Generator, Func<T5> t5Generator, Func<T6> t6Generator, Func<T7> t7Generator, Func<T8> t8Generator, Func<T9> t9Generator)
        {
            return () =>
            {
                return new MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>(t1Generator(), t2Generator(), t3Generator(), t4Generator(), t5Generator(), t6Generator(), t7Generator(), t8Generator(), t9Generator());
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>> Tuple10Generator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Func<T1> t1Generator, Func<T2> t2Generator, Func<T3> t3Generator, Func<T4> t4Generator, Func<T5> t5Generator, Func<T6> t6Generator, Func<T7> t7Generator, Func<T8> t8Generator, Func<T9> t9Generator, Func<T10> t10Generator)
        {
            return () =>
            {
                return new MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(t1Generator(), t2Generator(), t3Generator(), t4Generator(), t5Generator(), t6Generator(), t7Generator(), t8Generator(), t9Generator(), t10Generator());
            };
        }




        public static Func<MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>> Tuple11Generator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Func<T1> t1Generator, Func<T2> t2Generator, Func<T3> t3Generator, Func<T4> t4Generator, Func<T5> t5Generator, Func<T6> t6Generator, Func<T7> t7Generator, Func<T8> t8Generator, Func<T9> t9Generator, Func<T10> t10Generator, Func<T11> t11Generator)
        {
            return () =>
            {
                return new MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(t1Generator(), t2Generator(), t3Generator(), t4Generator(), t5Generator(), t6Generator(), t7Generator(), t8Generator(), t9Generator(), t10Generator(), t11Generator());
            };
        }




        public class MyTuple<T1> : Tuple<T1>
        {
            public MyTuple(T1 item1) : base(item1)
            {
            }
            public MyTuple<T1> Create(T1 item1)
            {
                return new MyTuple<T1>(item1);
            }
        }




        public class MyTuple<T1, T2> : Tuple<T1, T2>
        {
            public MyTuple(T1 item1, T2 item2) : base(item1, item2)
            {
            }
            public MyTuple<T1, T2> Create(T1 item1, T2 item2)
            {
                return new MyTuple<T1, T2>(item1, item2);
            }
        }




        public class MyTuple<T1, T2, T3> : Tuple<T1, T2, T3>
        {
            public MyTuple(T1 item1, T2 item2, T3 item3) : base(item1, item2, item3)
            {
            }
            public MyTuple<T1, T2, T3> Create(T1 item1, T2 item2, T3 item3)
            {
                return new MyTuple<T1, T2, T3>(item1, item2, item3);
            }
        }




        public class MyTuple<T1, T2, T3, T4> : Tuple<T1, T2, T3, T4>
        {
            public MyTuple(T1 item1, T2 item2, T3 item3, T4 item4) : base(item1, item2, item3, item4)
            {
            }
            public MyTuple<T1, T2, T3, T4> Create(T1 item1, T2 item2, T3 item3, T4 item4)
            {
                return new MyTuple<T1, T2, T3, T4>(item1, item2, item3, item4);
            }
        }




        public class MyTuple<T1, T2, T3, T4, T5> : Tuple<T1, T2, T3, T4, T5>
        {
            public MyTuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5) : base(item1, item2, item3, item4, item5)
            {
            }
            public MyTuple<T1, T2, T3, T4, T5> Create(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5)
            {
                return new MyTuple<T1, T2, T3, T4, T5>(item1, item2, item3, item4, item5);
            }
        }




        public class MyTuple<T1, T2, T3, T4, T5, T6> : Tuple<T1, T2, T3, T4, T5, T6>
        {
            public MyTuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6) : base(item1, item2, item3, item4, item5, item6)
            {
            }
            public MyTuple<T1, T2, T3, T4, T5, T6> Create(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6)
            {
                return new MyTuple<T1, T2, T3, T4, T5, T6>(item1, item2, item3, item4, item5, item6);
            }
        }




        public class MyTuple<T1, T2, T3, T4, T5, T6, T7> : Tuple<T1, T2, T3, T4, T5, T6, T7>
        {
            public MyTuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7) : base(item1, item2, item3, item4, item5, item6, item7)
            {
            }
            public MyTuple<T1, T2, T3, T4, T5, T6, T7> Create(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7)
            {
                return new MyTuple<T1, T2, T3, T4, T5, T6, T7>(item1, item2, item3, item4, item5, item6, item7);
            }
        }




        public class MyTuple<T1, T2, T3, T4, T5, T6, T7, T8> : Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8>>
        {
            public MyTuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, T8 item8) : base(item1, item2, item3, item4, item5, item6, item7, Tuple.Create(item8))
            {
            }
            public MyTuple<T1, T2, T3, T4, T5, T6, T7, T8> Create(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, T8 item8)
            {
                return new MyTuple<T1, T2, T3, T4, T5, T6, T7, T8>(item1, item2, item3, item4, item5, item6, item7, item8);
            }
        }




        public class MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9> : Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9>>
        {
            public MyTuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, T8 item8, T9 item9) : base(item1, item2, item3, item4, item5, item6, item7, Tuple.Create(item8, item9))
            {
            }
            public MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9> Create(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, T8 item8, T9 item9)
            {
                return new MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>(item1, item2, item3, item4, item5, item6, item7, item8, item9);
            }
        }




        public class MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> : Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10>>
        {
            public MyTuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, T8 item8, T9 item9, T10 item10) : base(item1, item2, item3, item4, item5, item6, item7, Tuple.Create(item8, item9, item10))
            {
            }
            public MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> Create(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, T8 item8, T9 item9, T10 item10)
            {
                return new MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(item1, item2, item3, item4, item5, item6, item7, item8, item9, item10);
            }
        }




        public class MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> : Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11>>
        {
            public MyTuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, T8 item8, T9 item9, T10 item10, T11 item11) : base(item1, item2, item3, item4, item5, item6, item7, Tuple.Create(item8, item9, item10, item11))
            {
            }
            public MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> Create(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, T8 item8, T9 item9, T10 item10, T11 item11)
            {
                return new MyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11);
            }
        }





        /*Auxiliary functions*/

        public class LocalRandom
        {
            private static System.Random random = new System.Random();
            public static int randomInt(int low, int high)
            {
                int num = random.Next(0, high);
                return num;
            }
        }

        public static T chooseFrom<T>(List<T> l)
        {
            System.Random random = new System.Random();
            int num = LocalRandom.randomInt(0, l.Count);
            return l[num];
        }




        public static Contract ContractGenerator()
        {
            return new Contract();
        }

        public static Never NeverGenerator()
        {
            throw new Exception("Never say never");
        }


        class ShakespeareGenerator
        {
            private static readonly string[] Quotes = new[] {
        "To be or not to be",
        "All the world's a stage",
        "To thine own self be true",
        "All that glitters is not gold",
        "There is nothing either good or bad",
        "To err is human, to forgive divine",
        "The lady doth protest too much",
        "To be, and not to be",
        "All's well that ends well",
        "A rose by any other name",
        "To sleep, perchance to dream",
        "To be, or not to be: that is the question"
    };

            public static string GetRandomQuote()
            {
                var random = new Random();
                var index = random.Next(Quotes.Length);
                return Quotes[index];
            }
        }


        public static string StringGenerator()
        {
            return ShakespeareGenerator.GetRandomQuote();
        }


        public static Nat NatGenerator()
        {
            return (BigInteger)(LocalRandom.randomInt(0, 10));
        }

        public static Int IntGenerator()
        {
            return (BigInteger)(LocalRandom.randomInt(0, 10));
        }

        public static Bytes BytesGenerator()
        {
            return Encoding.ASCII.GetBytes("ff");
        }

        public static Address AddressGenerator()
        {
            return "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb";
        }


        public static Signature SignatureGenerator()
        {
            return "edsigtkpiSSschcaCt9pUVrpNPf7TTcgvgDEDD6NCEHMy8NNQJCGnMfLZzYoQj74yLjo9wx6MPVV29CvVzgi7qEcEUok3k7AuMg";
        }


        public static Unit UnitGenerator()
        {
            return new Unit();
        }


        public static Bool BoolGenerator()
        {
            var myres = chooseFrom(new List<bool>() { true, false });
            return myres;
        }

        public static Timestamp TimestampGenerator()
        {
            return "2021-06-03T09:06:14.990-00:00";
        }

        public static KeyHash KeyHashGenerator()
        {
            return new KeyHash(AddressGenerator());
        }


        public static Key KeyGenerator()
        {
            return "edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav";
        }


        public static Tez TezGenerator()
        {
            return new Tez(IntGenerator());
        }

        public static Operation OperationGenerator()
        {
            return "op92MgaBZvgq2Rc4Aa4oTeZAjuAWHb63J2oEDP3bnFNWnGeja2T";
        }

        public static ChainId ChainIdGenerator()
        {
            return StringGenerator();
        }

        /*public static Sapling_state sapling_stateGenerator(){
                return StringGenerator();}*/


        /*public static Sapling_transaction_deprecated sapling_transaction_deprecatedGenerator(){
                return StringGenerator();}*/


        public static Func<Set<T>> SetGenerator<T>(Func<T> generator)
        {
            return () =>
            {
                var res = new Set<T>();
                int size = LocalRandom.randomInt(0, 5);
                for (int i = 0; i < size; i++)
                {
                    res.Add(generator());
                }
                return res;
            };
        }


        public static Func<List<T>> ListGenerator<T>(Func<T> generator)
        {
            return () =>
            {
                var res = new List<T>();
                int size = LocalRandom.randomInt(0, 5);
                for (int i = 0; i < size; i++)
                {
                    res.Add(generator());
                }
                return res;
            };
        }


        public static Func<Option<T>> OptionGenerator<T>(Func<T> generator) where T : notnull
        {
            var optres = generator();
            return () =>
            {
                return chooseFrom(new List<Option<T>> { new Some<T>(optres), new None<T>() });
            };
        }


        public static Ticket TicketGenerator()
        {
            return new Ticket();
        }

        public static Func<Map<TKey, TValue>> MapGenerator<TKey, TValue>(Func<TKey> keyGenerator, Func<TValue> valueGenerator) where TKey : notnull where TValue : notnull
        {
            return () => { return new Map<TKey, TValue>(DictionaryGenerator<TKey, TValue>(keyGenerator, valueGenerator)()); };
        }

        public static Func<BigMap<TKey, TValue>> BigMapGenerator<TKey, TValue>(Func<TKey> keyGenerator, Func<TValue> valueGenerator) where TKey : notnull where TValue : notnull
        {
            return () => { return new LiteralBigMap<TKey, TValue>(DictionaryGenerator<TKey, TValue>(keyGenerator, valueGenerator)()); };
        }


        public static Lambda LambdaGenerator()
        {
            return new Lambda();
        }
    }
}