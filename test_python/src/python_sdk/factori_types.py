from typing import (
    Type,
    Any,
    List,
    NewType,
    TypeVar,
    Dict,
    Callable,
    Optional,
    Union,
    Generic,
    Tuple,
)

from random import random
from math import floor
from pytezos.michelson.parse import michelson_to_micheline
from pytezos.michelson.types.base import MichelsonType
from pytezos.context.abstract import AbstractContext
from pytezos.michelson.types.domain import (
    KeyType,
    TimestampType,
    MutezType,
    SignatureType,
)
from pytezos.michelson.types.core import UnitType
from pytezos.michelson.types.core import NeverType
from pytezos.michelson.types.core import StringType
from pytezos.michelson.types.core import IntType
from pytezos.michelson.types.core import NatType
from pytezos.michelson.types.core import BytesType
from pytezos.michelson.types.core import BoolType
from pytezos.michelson.types.core import UnitLiteral, unit
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.option import OptionType
from pytezos.michelson.micheline import Micheline
from pytezos.michelson.types.list import ListType
from pytezos.michelson.types.set import SetType
from pytezos.michelson.types.map import MapType
from pytezos.michelson.types.big_map import BigMapType
from pytezos.michelson.types.sum import OrType
from pytezos.michelson.types.domain import KeyHashType
from pytezos.michelson.types.domain import ChainIdType
from pytezos.michelson.types.domain import ContractType
from pytezos.michelson.types.domain import LambdaType
from pytezos.michelson.types.operation import OperationType
from pytezos.michelson.types.ticket import TicketType


# Exceptions


class NeverHappens(Exception):
    pass


class NotElementOfType(Exception):
    pass


# Utils
T = TypeVar("T")


def decode_from_list(x_decode: Callable[[MichelsonType], T]) -> Callable[..., list[T]]:
    def result(l):
        res = []
        for x in l:
            res.append(x_decode(x))
        return res

    return result


def make_left(x, right_type=MichelsonType):
    return OrType.from_left(x, right_type)


def make_right(y, left_type=MichelsonType):
    return OrType.from_right(y, left_type)


# Bigmaps
# Abstract big maps are represented by an int
# Literal big maps are represented by a list of pairs of (key,value)


# Contract = NewType('Contract', str)

KT = TypeVar("KT")
VT = TypeVar("VT")


class Map(Generic[KT, VT]):
    def __init__(self, mydict=None):
        # Initialize the dictionary that will store the key-value pairs
        if mydict is None:
            mydict = {}
        self.dictionary = mydict

    def set(self, key: KT, value: VT):
        # Set the value for the given key in the dictionary
        self.dictionary[key] = value

    def get(self, key: KT) -> VT:
        # Return the value for the given key from the dictionary
        value: VT = self.dictionary[key]
        return value

    def is_empty(self) -> bool:
        # Return True if the dictionary is empty, False otherwise
        return len(self.dictionary) == 0

    def __eq__(self, other):
        # Check if the other object is a Map and if its dictionary is equal to this Map's dictionary
        return isinstance(other, Map) and self.dictionary == other.dictionary


class BigMap(Generic[KT, VT]):
    def __init__(self, value: Map[KT, VT] | int):
        self.value = value

    # Define the BigMap class as a subtype of Union, which can be either a Map or an int
    pass


class MyBigMapType(BigMapType):
    def __init__(
        self,
        items: List[Tuple[MichelsonType, MichelsonType]],
        ptr: Optional[int] = None,
        removed_keys: Optional[List[MichelsonType]] = None,
    ):
        super(BigMapType, self).__init__(items=items)
        self.ptr = ptr
        self.removed_keys = removed_keys or []
        self.context: Optional[AbstractContext] = None

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)

    @staticmethod
    def empty(
        key_type: Type[MichelsonType], val_type: Type[MichelsonType]
    ) -> "MyBigMapType":
        cls = MyBigMapType.create_type(args=[key_type, val_type])
        return cls(items=[])  # type: ignore

    def to_micheline_value(self, mode="readable", lazy_diff: Optional[bool] = False):
        # print("entering to_micheline_value!");
        if self.ptr is None:
            # print("self.ptr is None")
            if len(self.items) == 0:
                return []
            else:
                return [
                    {
                        "prim": "Elt",
                        "args": [k.to_micheline_value(), v.to_micheline_value()],
                    }
                    for k, v in self.items()
                ]
        else:
            # print("self.ptr is not None")
            return super().to_micheline_value(mode, lazy_diff)


#
# preBigMap = Union[Map[KT, VT], int]
#
#
# class BigMap(preBigMap):
#     pass


# Map = Dict[X, Y]


def string_encode(s: str) -> StringType:
    return StringType.from_value(s)


def string_decode(m: MichelsonType) -> str:
    if isinstance(m, StringType):
        return str(m)
    else:
        raise (ValueError("[string_decode] Not a String"))


assert string_decode(string_encode("coucou")) == "coucou"

address = str

signature = str


def signature_encode(s):
    return SignatureType(s)


def signature_decode(s: MichelsonType) -> str:
    if isinstance(s, SignatureType):
        return str(s)
    else:
        raise ValueError("Input is not a valid SignatureType")


key = str


def key_encode(k):
    return KeyType.from_value(k)


def key_decode(k: MichelsonType) -> key:
    if isinstance(k, KeyType):
        return key(k)
    else:
        raise ValueError("Input is not a valid KeyType")


key_hash = str


def key_hash_encode(k):
    return KeyHashType.from_value(k)


def key_hash_decode(k: MichelsonType) -> key_hash:
    if isinstance(k, KeyHashType):
        return key_hash(k)
    else:
        raise ValueError("Input is not a valid KeyHashType")


chain_id = str


def chain_id_encode(c):
    return ChainIdType.from_value(c)


def chain_id_decode(c: MichelsonType) -> str:
    if isinstance(c, ChainIdType):
        return str(c)
    else:
        raise ValueError("Input is not a valid ChainIdType")


timestamp = int

contract = str


def contract_encode(c: contract):
    try:
        return ContractType.from_value(c)
    except Exception as e:
        print("Error in contract_encode on input " + str(c) + " with error: " + str(e))
        raise e


def contract_decode(c: MichelsonType) -> Any:
    if isinstance(c, ContractType):
        try:
            return c.to_python_object()
        except Exception as e:
            print(
                "Error in contract_decode on input"
                + str(c.to_micheline_value())
                + " and error: "
                + str(e)
            )
            raise e
    else:
        raise ValueError("Input is not a valid ContractType")


Lambda = LambdaType


def lambda_encode(x=None, y=None):
    def result(l: Lambda) -> LambdaType:
        return l

    return result


def lambda_decode(x=None, y=None):
    def result(l: MichelsonType) -> Lambda:
        if isinstance(l, LambdaType):
            return l
        else:
            raise ValueError("Input is not a valid LambdaType")

    return result


Operation = OperationType


def operation_encode(o: Operation) -> OperationType:
    return o


def operation_decode(o: MichelsonType) -> Operation:
    if isinstance(o, OperationType):
        return o
    else:
        raise ValueError("Input is not a valid OperationType")


Ticket = TicketType


def ticket_encode(t: Ticket) -> TicketType:
    return t


def ticket_decode(t: MichelsonType) -> Ticket:
    if isinstance(t, TicketType):
        return t
    else:
        raise ValueError("Input is not a valid TicketType")


def timestamp_encode(t: timestamp):
    return TimestampType.from_value(t)


def timestamp_decode(t: MichelsonType) -> int:
    if isinstance(t, TimestampType):
        return int(t)
    else:
        raise ValueError("Input is not a valid TimestampType")


assert timestamp_decode(timestamp_encode(1702474034)) == 1702474034


tez = int


def tez_encode(t: tez):
    return MutezType.from_value(t)


def tez_decode(t: MichelsonType) -> tez:
    if isinstance(t, MutezType):
        res: tez = int(t)
        return res
    else:
        raise ValueError("Input is not a valid MutezType")


address_encode = string_encode

address_decode = string_decode


def int_encode(i: int) -> IntType:
    return IntType.from_value(i)


def int_decode(m: MichelsonType) -> int:
    if isinstance(m, IntType):
        return int(m)
    else:
        raise ValueError("Input is not a valid IntType")


# print("int_decode: " + str(int_decode(int_encode(42))))
assert int_decode(int_encode(42)) == 42

int_michelson = int

int_michelson_encode = int_encode
int_michelson_decode = int_decode

nat = int


def nat_encode(i: int) -> NatType:
    return NatType.from_value(i)


def nat_decode(m: MichelsonType) -> int:
    if isinstance(m, NatType):
        return int(m)
    else:
        raise ValueError("Input is not a valid NatType")


assert nat_decode(nat_encode(42)) == 42


def bytes_encode(b: bytes) -> BytesType:
    return BytesType.from_value(b)


def bytes_decode(m: MichelsonType) -> bytes:
    if isinstance(m, BytesType):
        return bytes(m)
    else:
        raise ValueError("Input is not a valid BytesType")


assert bytes_decode(bytes_encode(bytes.fromhex("ff"))) == bytes.fromhex("ff")


def bool_encode(b: bool) -> BoolType:
    return BoolType.from_value(b)


def bool_decode(m: MichelsonType) -> bool:
    if isinstance(m, BoolType):
        return bool(m)
    else:
        raise ValueError("Input is not a valid BoolType")


assert bool_decode(bool_encode(True)) == True
assert bool_decode(bool_encode(False)) == False


def unit_encode(u: Any) -> UnitType:
    return UnitType.dummy(u)


def unit_decode(m: MichelsonType) -> unit:
    if isinstance(m, UnitType):
        return unit()
    else:
        raise ValueError("Input is not a valid UnitType")


assert unit_decode(unit_encode(None)) == unit()
assert unit_decode(unit_encode([])) == unit()


def never_encode(x) -> NeverType:
    raise (NeverHappens("never say never"))


def never_decode(m: MichelsonType) -> Any:
    if isinstance(m, NeverType):
        raise (NeverHappens("never say never"))
    else:
        raise ValueError("Input is not a valid NeverType")


def option_encode(x_encode):
    def result(x: Optional[T]):
        if x is None:
            # TODO: check this is not a problem
            return OptionType.none(StringType)
        else:
            return OptionType.from_some(x_encode(x))

    return result


def option_decode(
    x_decode: Callable[[MichelsonType], T]
) -> Callable[[MichelsonType], Optional[T]]:
    def result(x: MichelsonType) -> Optional[T]:
        if isinstance(x, OptionType):
            if x.is_none():
                return None
            else:
                return x_decode(x.get_some())
        else:
            raise ValueError("Input is not a valid OptionType")

    return result


# def option_decode(x_decode):
#     def result(x: OptionType[T]):
#         if x.is_none():
#             return None
#         else:
#             return x.get_some()
#
#     return result


assert option_decode(int_decode)(option_encode(int_encode)(3)) == 3
assert option_decode(int_decode)(option_encode(int_encode)(None)) is None

# Michelson Key type
MKT = TypeVar("MKT")
# Michelson Value type
MVT = TypeVar("MVT")


def map_encode(
    k_encode: Callable[[KT], MichelsonType], v_encode: Callable[[VT], MichelsonType]
):
    def result(m: Map[KT, VT]):
        if m.is_empty():
            return MapType.empty(MichelsonType, MichelsonType)
        else:
            encoded_keys = map(k_encode, m.dictionary.keys())
            encoded_values = map(v_encode, m.dictionary.values())
            pre_res = list(zip(encoded_keys, encoded_values))
            res: MapType = MapType.from_items(pre_res)
            return res

    return result


test = map_encode(map_encode(int_encode, string_encode), int_encode)


#
# def map_encode(k_encode, v_encode):
#     def result(m: Map):
#         res = MapType.empty(X, Y)
#         for k in m.keys():
#             res = res.update(k_encode(k), v_encode(m[k]))
#         return res
#
#     return result


def map_decode(
    k_decode: Callable[[MichelsonType], KT], v_decode: Callable[[MichelsonType], VT]
):
    def result(m: MichelsonType) -> Map:
        if isinstance(m, MapType):
            preres: dict = {}
            for k, v in m.items:
                preres[k_decode(k)] = v_decode(v)
            res: Map = Map(preres)
            return res
        else:
            raise ValueError("Input is not a valid MapType")

    return result


# print(map_decode(int_decode, string_decode)(map_encode(int_encode, string_encode)(Map({1: 'mary'}))).dictionary)
assert map_decode(int_decode, string_decode)(
    map_encode(int_encode, string_encode)(Map({1: "mary"}))
) == Map({1: "mary"})


def big_map_encode(
    k_encode: Callable[[KT], MichelsonType], v_encode: Callable[[VT], MichelsonType]
):
    def result(m: BigMap[KT, VT]):
        if isinstance(m.value, int):
            # here we make the choice to return an empty bigmap to make deployment easier
            return MyBigMapType.empty(MichelsonType, MichelsonType)
        elif isinstance(m.value, Map):
            if m.value.is_empty():
                return MyBigMapType.empty(MichelsonType, MichelsonType)
            else:
                encoded_keys = map(k_encode, m.value.dictionary.keys())
                encoded_values = map(v_encode, m.value.dictionary.values())
                pre_res = list(zip(encoded_keys, encoded_values))
                res = MyBigMapType(items=pre_res)
                return res

    return result


def big_map_decode(
    k_decode: Callable[[MichelsonType], KT], v_decode: Callable[[MichelsonType], VT]
):
    def result(m: MichelsonType) -> BigMap:
        if isinstance(m, MyBigMapType):
            if m.ptr is not None:
                return BigMap(int(m.ptr))
            preres: dict = {}
            for k, v in m.items:
                preres[k_decode(k)] = v_decode(v)
            res: BigMap = BigMap(Map(preres))
            return res
        else:
            raise ValueError("Input is not a valid MyBigMapType")

    return result


def list_encode(x_encode):
    def result(l) -> ListType:
        lres = []
        for i in l:
            lres.append(x_encode(i))
        res = ListType(lres)  # I suspect this will fail on empty lists TODO
        return res

    return result


def list_decode(x_decode: Callable[[MichelsonType], T]):
    def result(l: MichelsonType) -> List[T]:
        if isinstance(l, ListType):
            return decode_from_list(x_decode)(l)
        else:
            raise ValueError("Input is not a valid ListType")

    return result


toto = list_encode(int_encode)([1, 2, 3, 4])
tata = list_decode(int_decode)(toto)
assert list_decode(int_decode)(list_encode(int_encode)([1, 2, 3, 4])) == [1, 2, 3, 4]


class Set(Generic[T]):
    def __init__(self, mylist: List[T]):
        self.list = mylist


def set_encode(x_encode: Callable[[T], MichelsonType]):
    def result(x: Set[T]) -> SetType:
        res: SetType = SetType([x_encode(i) for i in x.list])
        return res


def set_decode(x_decode: Callable[[MichelsonType], T]):
    def result(x: MichelsonType) -> Set:
        if isinstance(x, SetType):
            preres = [x_decode(i) for i in x.items]
            return Set(preres)
        else:
            raise ValueError("Input is not a valid SetType")

    return result


def tuple2_encode(x_encode, y_encode):
    def result(arg) -> PairType:
        x = x_encode(arg[0])
        # print("[tuple2_encode] x : " + str(x))
        y = y_encode(arg[1])
        return PairType.from_comb([x, y])

    return result


def tuple2_decode(x_decode, y_decode):
    def result(p: MichelsonType) -> Tuple:
        if isinstance(p, PairType):
            x = p.items[0]
            y = p.items[1]
            res = (x_decode(x), y_decode(y))
            return res
        else:
            raise ValueError("Input is not a valid PairType")

    return result


#     tuple3_encode(string_encode, int_encode, bytes_encode)("toto", 3, bytes.fromhex('ff'))) == ["toto", 3,
#                                                                                                 bytes.fromhex('ff')])


# x1 = int_encode(2)
# print("x1 : " + str(x1))
# x2 = int_encode(3)
# x3 = int_encode(4)
# p1 = PairType.from_comb([x1, x2, x3])
# ep1 = tuple2_encode(string_encode, int_encode)("toto", 3)
# dp1 = tuple2_decode(string_decode, int_decode)(ep1)

# print(dp1)
# res = (tuple2_encode(int_encode, int_encode)(2, 3))
# print(res)
# resinv = tuple2_decode(string_decode, int_decode)(res)
# print(resinv)


assert tuple2_decode(string_decode, int_decode)(
    tuple2_encode(string_encode, int_encode)(("toto", 3))
) == ("toto", 3)


#
# # The from_comb is probably not the right way to build a pair with three elements
# def tuple3_encode(x_encode, y_encode, z_encode):
#     def result(x, y, z) -> PairType:
#         x = x_encode(x)
#         y = y_encode(y)
#         z = z_encode(z)
#         return PairType.from_comb([x, y, z])
#
#     return result
#
#
# # This doesn't type well and there's a chance it won't work on data from the blockchain.. TODO: investigate
# def tuple3_decode(x_decode, y_decode, z_decode):
#     def result(p: PairType):
#         x = p.items[0]
#         rest = tuple2_decode(y_decode, z_decode)((p.items[1]))
#         rest.insert(0, x_decode(x))
#         return rest
#
#     return result


# print(tuple3_decode(string_decode, int_decode, bytes_decode)(
#     tuple3_encode(string_encode, int_encode, bytes_encode)("toto", 3, bytes.fromhex('ff'))))
# assert (tuple3_decode(string_decode, int_decode, bytes_decode)(


def myrandom(mn: int, mx: int):
    r = random() * (mx - mn) + mn
    res = floor(r)
    return res


def chooseFrom(l: List[T]):
    mylist = l
    n = len(mylist)
    index = myrandom(0, n)
    chosenFun = mylist[index]
    return chosenFun


def int64_generator():
    return myrandom(0, 10)


def make_list(my_generator: Callable[[], T], k: int):
    res = list(range(k))
    for i in range(k):
        res[i] = my_generator()

    def result() -> list[T]:
        return res

    return result


T1 = TypeVar("T1")
T2 = TypeVar("T2")
T3 = TypeVar("T3")
T4 = TypeVar("T4")
T5 = TypeVar("T5")
T6 = TypeVar("T6")
T7 = TypeVar("T7")
T8 = TypeVar("T8")
T9 = TypeVar("T9")
T10 = TypeVar("T10")
T11 = TypeVar("T11")
T12 = TypeVar("T12")
T13 = TypeVar("T13")
T14 = TypeVar("T14")
T15 = TypeVar("T15")
T16 = TypeVar("T16")
T17 = TypeVar("T17")
T18 = TypeVar("T18")
T19 = TypeVar("T19")
T20 = TypeVar("T20")
T21 = TypeVar("T21")
T22 = TypeVar("T22")
T23 = TypeVar("T23")
T24 = TypeVar("T24")
T25 = TypeVar("T25")


def contract_generator() -> contract:
    return "KT1G7ziTpUgXQR9QymGj348jM5B8KdZgBp1B"  # random KT1 pulled from ghostnet


def never_generator() -> NeverType:
    raise Exception("Never gonna generate a `never`")


def string_generator() -> str:
    return "Very like a whale"


def nat_generator() -> nat:
    return int64_generator()


def int_generator() -> int:
    return myrandom(0, 10)


def bytes_generator():
    return bytes.fromhex("ff")


def address_generator():
    return "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"


def signature_generator():
    return "edsigtkpiSSschcaCt9pUVrpNPf7TTcgvgDEDD6NCEHMy8NNQJCGnMfLZzYoQj74yLjo9wx6MPVV29CvVzgi7qEcEUok3k7AuMg"


def unit_generator():
    return unit()


def bool_generator() -> bool:
    myres = chooseFrom([True, False])
    return myres


def timestamp_generator() -> timestamp:
    return 1702474034


def key_hash_generator():
    return address_generator()


def key_generator():
    return "edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav"


def tez_generator() -> tez:
    return int64_generator()


def operation_generator():
    return "op92MgaBZvgq2Rc4Aa4oTeZAjuAWHb63J2oEDP3bnFNWnGeja2T"


def chain_id_generator():
    return string_generator()


def sapling_state_generator():
    return string_generator()


def sapling_transaction_deprecated_generator():
    return string_generator()


def set_generator(f: Any):
    return list_generator(f)


def list_generator(f: Callable[[], T]):
    res = make_list(f, int_generator())()

    def result() -> list[T]:
        return res

    return result


def option_generator(f: Any):
    optres = f()

    def result():
        return chooseFrom([(lambda: None), (lambda: optres)])()

    return result


def ticket_generator(f):
    def result() -> Ticket:
        res = f()
        return TicketType.dummy(AbstractContext())

    return result


def map_generator(f: Any, g: Any):
    def result():
        return Map({})

    return result


def big_map_generator(f, g):
    def result():
        return BigMap(Map({}))

    return result


def lambda_generator(f: Any, g: Any):
    def result():
        lmbd = LambdaType.match(
            michelson_to_micheline("lambda int int")
        ).from_micheline_value(michelson_to_micheline("{ DROP ; UNIT ; FAILWITH }"))
        return lmbd

    return result


def tuple2_generator(
    f1: Callable[[], T1], f2: Callable[[], T2]
) -> Callable[[], Tuple[T1, T2]]:
    def result() -> Tuple[T1, T2]:
        return f1(), f2()

    return result


def tuple3_generator(
    f1: Callable[[], T1], f2: Callable[[], T2], f3: Callable[[], T3]
) -> Callable[[], Tuple[T1, T2, T3]]:
    def result() -> Tuple[T1, T2, T3]:
        return f1(), f2(), f3()

    return result


def tuple4_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
) -> Callable[[], Tuple[T1, T2, T3, T4]]:
    def result() -> Tuple[T1, T2, T3, T4]:
        return f1(), f2(), f3(), f4()

    return result


def tuple5_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
) -> Callable[[], Tuple[T1, T2, T3, T4, T5]]:
    def result() -> Tuple[T1, T2, T3, T4, T5]:
        return f1(), f2(), f3(), f4(), f5()

    return result


def tuple6_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
) -> Callable[[], Tuple[T1, T2, T3, T4, T5, T6]]:
    def result() -> Tuple[T1, T2, T3, T4, T5, T6]:
        return f1(), f2(), f3(), f4(), f5(), f6()

    return result


def tuple7_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
) -> Callable[[], Tuple[T1, T2, T3, T4, T5, T6, T7]]:
    def result() -> Tuple[T1, T2, T3, T4, T5, T6, T7]:
        return f1(), f2(), f3(), f4(), f5(), f6(), f7()

    return result


def tuple8_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
) -> Callable[[], Tuple[T1, T2, T3, T4, T5, T6, T7, T8]]:
    def result() -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8]:
        return f1(), f2(), f3(), f4(), f5(), f6(), f7(), f8()

    return result


def tuple9_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
) -> Callable[[], Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9]]:
    def result() -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9]:
        return f1(), f2(), f3(), f4(), f5(), f6(), f7(), f8(), f9()

    return result


def tuple10_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
) -> Callable[[], Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10]]:
    def result() -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10]:
        return f1(), f2(), f3(), f4(), f5(), f6(), f7(), f8(), f9(), f10()

    return result


def tuple11_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
) -> Callable[[], Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11]]:
    def result() -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11]:
        return f1(), f2(), f3(), f4(), f5(), f6(), f7(), f8(), f9(), f10(), f11()

    return result


def tuple12_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
) -> Callable[[], Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12]]:
    def result() -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12]:
        return f1(), f2(), f3(), f4(), f5(), f6(), f7(), f8(), f9(), f10(), f11(), f12()

    return result


def tuple13_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
    f13: Callable[[], T13],
) -> Callable[[], Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13]]:
    def result() -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13]:
        return (
            f1(),
            f2(),
            f3(),
            f4(),
            f5(),
            f6(),
            f7(),
            f8(),
            f9(),
            f10(),
            f11(),
            f12(),
            f13(),
        )

    return result


def tuple14_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
    f13: Callable[[], T13],
    f14: Callable[[], T14],
) -> Callable[[], Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14]]:
    def result() -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14]:
        return (
            f1(),
            f2(),
            f3(),
            f4(),
            f5(),
            f6(),
            f7(),
            f8(),
            f9(),
            f10(),
            f11(),
            f12(),
            f13(),
            f14(),
        )

    return result


def tuple15_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
    f13: Callable[[], T13],
    f14: Callable[[], T14],
    f15: Callable[[], T15],
) -> Callable[
    [], Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15]
]:
    def result() -> (
        Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15]
    ):
        return (
            f1(),
            f2(),
            f3(),
            f4(),
            f5(),
            f6(),
            f7(),
            f8(),
            f9(),
            f10(),
            f11(),
            f12(),
            f13(),
            f14(),
            f15(),
        )

    return result


def tuple16_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
    f13: Callable[[], T13],
    f14: Callable[[], T14],
    f15: Callable[[], T15],
    f16: Callable[[], T16],
) -> Callable[
    [], Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16]
]:
    def result() -> (
        Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16]
    ):
        return (
            f1(),
            f2(),
            f3(),
            f4(),
            f5(),
            f6(),
            f7(),
            f8(),
            f9(),
            f10(),
            f11(),
            f12(),
            f13(),
            f14(),
            f15(),
            f16(),
        )

    return result


def tuple17_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
    f13: Callable[[], T13],
    f14: Callable[[], T14],
    f15: Callable[[], T15],
    f16: Callable[[], T16],
    f17: Callable[[], T17],
) -> Callable[
    [],
    Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17],
]:
    def result() -> (
        Tuple[
            T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17
        ]
    ):
        return (
            f1(),
            f2(),
            f3(),
            f4(),
            f5(),
            f6(),
            f7(),
            f8(),
            f9(),
            f10(),
            f11(),
            f12(),
            f13(),
            f14(),
            f15(),
            f16(),
            f17(),
        )

    return result


def tuple18_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
    f13: Callable[[], T13],
    f14: Callable[[], T14],
    f15: Callable[[], T15],
    f16: Callable[[], T16],
    f17: Callable[[], T17],
    f18: Callable[[], T18],
) -> Callable[
    [],
    Tuple[
        T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18
    ],
]:
    def result() -> (
        Tuple[
            T1,
            T2,
            T3,
            T4,
            T5,
            T6,
            T7,
            T8,
            T9,
            T10,
            T11,
            T12,
            T13,
            T14,
            T15,
            T16,
            T17,
            T18,
        ]
    ):
        return (
            f1(),
            f2(),
            f3(),
            f4(),
            f5(),
            f6(),
            f7(),
            f8(),
            f9(),
            f10(),
            f11(),
            f12(),
            f13(),
            f14(),
            f15(),
            f16(),
            f17(),
            f18(),
        )

    return result


def tuple19_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
    f13: Callable[[], T13],
    f14: Callable[[], T14],
    f15: Callable[[], T15],
    f16: Callable[[], T16],
    f17: Callable[[], T17],
    f18: Callable[[], T18],
    f19: Callable[[], T19],
) -> Callable[
    [],
    Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
    ],
]:
    def result() -> (
        Tuple[
            T1,
            T2,
            T3,
            T4,
            T5,
            T6,
            T7,
            T8,
            T9,
            T10,
            T11,
            T12,
            T13,
            T14,
            T15,
            T16,
            T17,
            T18,
            T19,
        ]
    ):
        return (
            f1(),
            f2(),
            f3(),
            f4(),
            f5(),
            f6(),
            f7(),
            f8(),
            f9(),
            f10(),
            f11(),
            f12(),
            f13(),
            f14(),
            f15(),
            f16(),
            f17(),
            f18(),
            f19(),
        )

    return result


def tuple20_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
    f13: Callable[[], T13],
    f14: Callable[[], T14],
    f15: Callable[[], T15],
    f16: Callable[[], T16],
    f17: Callable[[], T17],
    f18: Callable[[], T18],
    f19: Callable[[], T19],
    f20: Callable[[], T20],
) -> Callable[
    [],
    Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
    ],
]:
    def result() -> (
        Tuple[
            T1,
            T2,
            T3,
            T4,
            T5,
            T6,
            T7,
            T8,
            T9,
            T10,
            T11,
            T12,
            T13,
            T14,
            T15,
            T16,
            T17,
            T18,
            T19,
            T20,
        ]
    ):
        return (
            f1(),
            f2(),
            f3(),
            f4(),
            f5(),
            f6(),
            f7(),
            f8(),
            f9(),
            f10(),
            f11(),
            f12(),
            f13(),
            f14(),
            f15(),
            f16(),
            f17(),
            f18(),
            f19(),
            f20(),
        )

    return result


def tuple21_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
    f13: Callable[[], T13],
    f14: Callable[[], T14],
    f15: Callable[[], T15],
    f16: Callable[[], T16],
    f17: Callable[[], T17],
    f18: Callable[[], T18],
    f19: Callable[[], T19],
    f20: Callable[[], T20],
    f21: Callable[[], T21],
) -> Callable[
    [],
    Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
        T21,
    ],
]:
    def result() -> (
        Tuple[
            T1,
            T2,
            T3,
            T4,
            T5,
            T6,
            T7,
            T8,
            T9,
            T10,
            T11,
            T12,
            T13,
            T14,
            T15,
            T16,
            T17,
            T18,
            T19,
            T20,
            T21,
        ]
    ):
        return (
            f1(),
            f2(),
            f3(),
            f4(),
            f5(),
            f6(),
            f7(),
            f8(),
            f9(),
            f10(),
            f11(),
            f12(),
            f13(),
            f14(),
            f15(),
            f16(),
            f17(),
            f18(),
            f19(),
            f20(),
            f21(),
        )

    return result


def tuple22_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
    f13: Callable[[], T13],
    f14: Callable[[], T14],
    f15: Callable[[], T15],
    f16: Callable[[], T16],
    f17: Callable[[], T17],
    f18: Callable[[], T18],
    f19: Callable[[], T19],
    f20: Callable[[], T20],
    f21: Callable[[], T21],
    f22: Callable[[], T22],
) -> Callable[
    [],
    Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
        T21,
        T22,
    ],
]:
    def result() -> (
        Tuple[
            T1,
            T2,
            T3,
            T4,
            T5,
            T6,
            T7,
            T8,
            T9,
            T10,
            T11,
            T12,
            T13,
            T14,
            T15,
            T16,
            T17,
            T18,
            T19,
            T20,
            T21,
            T22,
        ]
    ):
        return (
            f1(),
            f2(),
            f3(),
            f4(),
            f5(),
            f6(),
            f7(),
            f8(),
            f9(),
            f10(),
            f11(),
            f12(),
            f13(),
            f14(),
            f15(),
            f16(),
            f17(),
            f18(),
            f19(),
            f20(),
            f21(),
            f22(),
        )

    return result


def tuple23_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
    f13: Callable[[], T13],
    f14: Callable[[], T14],
    f15: Callable[[], T15],
    f16: Callable[[], T16],
    f17: Callable[[], T17],
    f18: Callable[[], T18],
    f19: Callable[[], T19],
    f20: Callable[[], T20],
    f21: Callable[[], T21],
    f22: Callable[[], T22],
    f23: Callable[[], T23],
) -> Callable[
    [],
    Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
        T21,
        T22,
        T23,
    ],
]:
    def result() -> (
        Tuple[
            T1,
            T2,
            T3,
            T4,
            T5,
            T6,
            T7,
            T8,
            T9,
            T10,
            T11,
            T12,
            T13,
            T14,
            T15,
            T16,
            T17,
            T18,
            T19,
            T20,
            T21,
            T22,
            T23,
        ]
    ):
        return (
            f1(),
            f2(),
            f3(),
            f4(),
            f5(),
            f6(),
            f7(),
            f8(),
            f9(),
            f10(),
            f11(),
            f12(),
            f13(),
            f14(),
            f15(),
            f16(),
            f17(),
            f18(),
            f19(),
            f20(),
            f21(),
            f22(),
            f23(),
        )

    return result


def tuple24_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
    f13: Callable[[], T13],
    f14: Callable[[], T14],
    f15: Callable[[], T15],
    f16: Callable[[], T16],
    f17: Callable[[], T17],
    f18: Callable[[], T18],
    f19: Callable[[], T19],
    f20: Callable[[], T20],
    f21: Callable[[], T21],
    f22: Callable[[], T22],
    f23: Callable[[], T23],
    f24: Callable[[], T24],
) -> Callable[
    [],
    Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
        T21,
        T22,
        T23,
        T24,
    ],
]:
    def result() -> (
        Tuple[
            T1,
            T2,
            T3,
            T4,
            T5,
            T6,
            T7,
            T8,
            T9,
            T10,
            T11,
            T12,
            T13,
            T14,
            T15,
            T16,
            T17,
            T18,
            T19,
            T20,
            T21,
            T22,
            T23,
            T24,
        ]
    ):
        return (
            f1(),
            f2(),
            f3(),
            f4(),
            f5(),
            f6(),
            f7(),
            f8(),
            f9(),
            f10(),
            f11(),
            f12(),
            f13(),
            f14(),
            f15(),
            f16(),
            f17(),
            f18(),
            f19(),
            f20(),
            f21(),
            f22(),
            f23(),
            f24(),
        )

    return result


def tuple25_generator(
    f1: Callable[[], T1],
    f2: Callable[[], T2],
    f3: Callable[[], T3],
    f4: Callable[[], T4],
    f5: Callable[[], T5],
    f6: Callable[[], T6],
    f7: Callable[[], T7],
    f8: Callable[[], T8],
    f9: Callable[[], T9],
    f10: Callable[[], T10],
    f11: Callable[[], T11],
    f12: Callable[[], T12],
    f13: Callable[[], T13],
    f14: Callable[[], T14],
    f15: Callable[[], T15],
    f16: Callable[[], T16],
    f17: Callable[[], T17],
    f18: Callable[[], T18],
    f19: Callable[[], T19],
    f20: Callable[[], T20],
    f21: Callable[[], T21],
    f22: Callable[[], T22],
    f23: Callable[[], T23],
    f24: Callable[[], T24],
    f25: Callable[[], T25],
) -> Callable[
    [],
    Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
        T21,
        T22,
        T23,
        T24,
        T25,
    ],
]:
    def result() -> (
        Tuple[
            T1,
            T2,
            T3,
            T4,
            T5,
            T6,
            T7,
            T8,
            T9,
            T10,
            T11,
            T12,
            T13,
            T14,
            T15,
            T16,
            T17,
            T18,
            T19,
            T20,
            T21,
            T22,
            T23,
            T24,
            T25,
        ]
    ):
        return (
            f1(),
            f2(),
            f3(),
            f4(),
            f5(),
            f6(),
            f7(),
            f8(),
            f9(),
            f10(),
            f11(),
            f12(),
            f13(),
            f14(),
            f15(),
            f16(),
            f17(),
            f18(),
            f19(),
            f20(),
            f21(),
            f22(),
            f23(),
            f24(),
            f25(),
        )

    return result


def tuple3_encode(enc1, enc2, enc3):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb([enc1(arg[0]), enc2(arg[1]), enc3(arg[2])])
        except Exception as e:
            print("Error in tuple3_encode")
            print(e)
            raise (e)

    return result


def tuple4_encode(enc1, enc2, enc3, enc4):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [enc1(arg[0]), enc2(arg[1]), enc3(arg[2]), enc4(arg[3])]
            )
        except Exception as e:
            print("Error in tuple4_encode")
            print(e)
            raise (e)

    return result


def tuple5_encode(enc1, enc2, enc3, enc4, enc5):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [enc1(arg[0]), enc2(arg[1]), enc3(arg[2]), enc4(arg[3]), enc5(arg[4])]
            )
        except Exception as e:
            print("Error in tuple5_encode")
            print(e)
            raise (e)

    return result


def tuple6_encode(enc1, enc2, enc3, enc4, enc5, enc6):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                ]
            )
        except Exception as e:
            print("Error in tuple6_encode")
            print(e)
            raise (e)

    return result


def tuple7_encode(enc1, enc2, enc3, enc4, enc5, enc6, enc7):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                ]
            )
        except Exception as e:
            print("Error in tuple7_encode")
            print(e)
            raise (e)

    return result


def tuple8_encode(enc1, enc2, enc3, enc4, enc5, enc6, enc7, enc8):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                ]
            )
        except Exception as e:
            print("Error in tuple8_encode")
            print(e)
            raise (e)

    return result


def tuple9_encode(enc1, enc2, enc3, enc4, enc5, enc6, enc7, enc8, enc9):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                ]
            )
        except Exception as e:
            print("Error in tuple9_encode")
            print(e)
            raise (e)

    return result


def tuple10_encode(enc1, enc2, enc3, enc4, enc5, enc6, enc7, enc8, enc9, enc10):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                ]
            )
        except Exception as e:
            print("Error in tuple10_encode")
            print(e)
            raise (e)

    return result


def tuple11_encode(enc1, enc2, enc3, enc4, enc5, enc6, enc7, enc8, enc9, enc10, enc11):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                ]
            )
        except Exception as e:
            print("Error in tuple11_encode")
            print(e)
            raise (e)

    return result


def tuple12_encode(
    enc1, enc2, enc3, enc4, enc5, enc6, enc7, enc8, enc9, enc10, enc11, enc12
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                ]
            )
        except Exception as e:
            print("Error in tuple12_encode")
            print(e)
            raise (e)

    return result


def tuple13_encode(
    enc1, enc2, enc3, enc4, enc5, enc6, enc7, enc8, enc9, enc10, enc11, enc12, enc13
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                    enc13(arg[12]),
                ]
            )
        except Exception as e:
            print("Error in tuple13_encode")
            print(e)
            raise (e)

    return result


def tuple14_encode(
    enc1,
    enc2,
    enc3,
    enc4,
    enc5,
    enc6,
    enc7,
    enc8,
    enc9,
    enc10,
    enc11,
    enc12,
    enc13,
    enc14,
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                    enc13(arg[12]),
                    enc14(arg[13]),
                ]
            )
        except Exception as e:
            print("Error in tuple14_encode")
            print(e)
            raise (e)

    return result


def tuple15_encode(
    enc1,
    enc2,
    enc3,
    enc4,
    enc5,
    enc6,
    enc7,
    enc8,
    enc9,
    enc10,
    enc11,
    enc12,
    enc13,
    enc14,
    enc15,
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                    enc13(arg[12]),
                    enc14(arg[13]),
                    enc15(arg[14]),
                ]
            )
        except Exception as e:
            print("Error in tuple15_encode")
            print(e)
            raise (e)

    return result


def tuple16_encode(
    enc1,
    enc2,
    enc3,
    enc4,
    enc5,
    enc6,
    enc7,
    enc8,
    enc9,
    enc10,
    enc11,
    enc12,
    enc13,
    enc14,
    enc15,
    enc16,
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                    enc13(arg[12]),
                    enc14(arg[13]),
                    enc15(arg[14]),
                    enc16(arg[15]),
                ]
            )
        except Exception as e:
            print("Error in tuple16_encode")
            print(e)
            raise (e)

    return result


def tuple17_encode(
    enc1,
    enc2,
    enc3,
    enc4,
    enc5,
    enc6,
    enc7,
    enc8,
    enc9,
    enc10,
    enc11,
    enc12,
    enc13,
    enc14,
    enc15,
    enc16,
    enc17,
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                    enc13(arg[12]),
                    enc14(arg[13]),
                    enc15(arg[14]),
                    enc16(arg[15]),
                    enc17(arg[16]),
                ]
            )
        except Exception as e:
            print("Error in tuple17_encode")
            print(e)
            raise (e)

    return result


def tuple18_encode(
    enc1,
    enc2,
    enc3,
    enc4,
    enc5,
    enc6,
    enc7,
    enc8,
    enc9,
    enc10,
    enc11,
    enc12,
    enc13,
    enc14,
    enc15,
    enc16,
    enc17,
    enc18,
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                    enc13(arg[12]),
                    enc14(arg[13]),
                    enc15(arg[14]),
                    enc16(arg[15]),
                    enc17(arg[16]),
                    enc18(arg[17]),
                ]
            )
        except Exception as e:
            print("Error in tuple18_encode")
            print(e)
            raise (e)

    return result


def tuple19_encode(
    enc1,
    enc2,
    enc3,
    enc4,
    enc5,
    enc6,
    enc7,
    enc8,
    enc9,
    enc10,
    enc11,
    enc12,
    enc13,
    enc14,
    enc15,
    enc16,
    enc17,
    enc18,
    enc19,
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                    enc13(arg[12]),
                    enc14(arg[13]),
                    enc15(arg[14]),
                    enc16(arg[15]),
                    enc17(arg[16]),
                    enc18(arg[17]),
                    enc19(arg[18]),
                ]
            )
        except Exception as e:
            print("Error in tuple19_encode")
            print(e)
            raise (e)

    return result


def tuple20_encode(
    enc1,
    enc2,
    enc3,
    enc4,
    enc5,
    enc6,
    enc7,
    enc8,
    enc9,
    enc10,
    enc11,
    enc12,
    enc13,
    enc14,
    enc15,
    enc16,
    enc17,
    enc18,
    enc19,
    enc20,
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                    enc13(arg[12]),
                    enc14(arg[13]),
                    enc15(arg[14]),
                    enc16(arg[15]),
                    enc17(arg[16]),
                    enc18(arg[17]),
                    enc19(arg[18]),
                    enc20(arg[19]),
                ]
            )
        except Exception as e:
            print("Error in tuple20_encode")
            print(e)
            raise (e)

    return result


def tuple21_encode(
    enc1,
    enc2,
    enc3,
    enc4,
    enc5,
    enc6,
    enc7,
    enc8,
    enc9,
    enc10,
    enc11,
    enc12,
    enc13,
    enc14,
    enc15,
    enc16,
    enc17,
    enc18,
    enc19,
    enc20,
    enc21,
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                    enc13(arg[12]),
                    enc14(arg[13]),
                    enc15(arg[14]),
                    enc16(arg[15]),
                    enc17(arg[16]),
                    enc18(arg[17]),
                    enc19(arg[18]),
                    enc20(arg[19]),
                    enc21(arg[20]),
                ]
            )
        except Exception as e:
            print("Error in tuple21_encode")
            print(e)
            raise (e)

    return result


def tuple22_encode(
    enc1,
    enc2,
    enc3,
    enc4,
    enc5,
    enc6,
    enc7,
    enc8,
    enc9,
    enc10,
    enc11,
    enc12,
    enc13,
    enc14,
    enc15,
    enc16,
    enc17,
    enc18,
    enc19,
    enc20,
    enc21,
    enc22,
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                    enc13(arg[12]),
                    enc14(arg[13]),
                    enc15(arg[14]),
                    enc16(arg[15]),
                    enc17(arg[16]),
                    enc18(arg[17]),
                    enc19(arg[18]),
                    enc20(arg[19]),
                    enc21(arg[20]),
                    enc22(arg[21]),
                ]
            )
        except Exception as e:
            print("Error in tuple22_encode")
            print(e)
            raise (e)

    return result


def tuple23_encode(
    enc1,
    enc2,
    enc3,
    enc4,
    enc5,
    enc6,
    enc7,
    enc8,
    enc9,
    enc10,
    enc11,
    enc12,
    enc13,
    enc14,
    enc15,
    enc16,
    enc17,
    enc18,
    enc19,
    enc20,
    enc21,
    enc22,
    enc23,
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                    enc13(arg[12]),
                    enc14(arg[13]),
                    enc15(arg[14]),
                    enc16(arg[15]),
                    enc17(arg[16]),
                    enc18(arg[17]),
                    enc19(arg[18]),
                    enc20(arg[19]),
                    enc21(arg[20]),
                    enc22(arg[21]),
                    enc23(arg[22]),
                ]
            )
        except Exception as e:
            print("Error in tuple23_encode")
            print(e)
            raise (e)

    return result


def tuple24_encode(
    enc1,
    enc2,
    enc3,
    enc4,
    enc5,
    enc6,
    enc7,
    enc8,
    enc9,
    enc10,
    enc11,
    enc12,
    enc13,
    enc14,
    enc15,
    enc16,
    enc17,
    enc18,
    enc19,
    enc20,
    enc21,
    enc22,
    enc23,
    enc24,
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                    enc13(arg[12]),
                    enc14(arg[13]),
                    enc15(arg[14]),
                    enc16(arg[15]),
                    enc17(arg[16]),
                    enc18(arg[17]),
                    enc19(arg[18]),
                    enc20(arg[19]),
                    enc21(arg[20]),
                    enc22(arg[21]),
                    enc23(arg[22]),
                    enc24(arg[23]),
                ]
            )
        except Exception as e:
            print("Error in tuple24_encode")
            print(e)
            raise (e)

    return result


def tuple25_encode(
    enc1,
    enc2,
    enc3,
    enc4,
    enc5,
    enc6,
    enc7,
    enc8,
    enc9,
    enc10,
    enc11,
    enc12,
    enc13,
    enc14,
    enc15,
    enc16,
    enc17,
    enc18,
    enc19,
    enc20,
    enc21,
    enc22,
    enc23,
    enc24,
    enc25,
):
    def result(arg: Tuple) -> PairType:
        try:
            return PairType.from_comb(
                [
                    enc1(arg[0]),
                    enc2(arg[1]),
                    enc3(arg[2]),
                    enc4(arg[3]),
                    enc5(arg[4]),
                    enc6(arg[5]),
                    enc7(arg[6]),
                    enc8(arg[7]),
                    enc9(arg[8]),
                    enc10(arg[9]),
                    enc11(arg[10]),
                    enc12(arg[11]),
                    enc13(arg[12]),
                    enc14(arg[13]),
                    enc15(arg[14]),
                    enc16(arg[15]),
                    enc17(arg[16]),
                    enc18(arg[17]),
                    enc19(arg[18]),
                    enc20(arg[19]),
                    enc21(arg[20]),
                    enc22(arg[21]),
                    enc23(arg[22]),
                    enc24(arg[23]),
                    enc25(arg[24]),
                ]
            )
        except Exception as e:
            print("Error in tuple25_encode")
            print(e)
            raise (e)

    return result


def tuple3_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
):
    def result(p) -> Tuple[T1, T2, T3]:
        try:
            x1 = p.items[0]
            prerest: Tuple[T2, T3] = tuple2_decode(dec2, dec3)(p.items[1])
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[T1, T2, T3] = first + prerest
            return rest
        except Exception as e:
            print("Error in tuple3_decode")
            print(e)
            raise e

    return result


assert tuple3_decode(int_decode, int_decode, int_decode)(
    tuple3_encode(int_encode, int_encode, int_encode)((1, 2, 3))
) == (1, 2, 3)


def tuple4_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
):
    def result(p) -> Tuple[T1, T2, T3, T4]:
        try:
            x1 = p.items[0]
            prerest: Tuple[T2, T3, T4] = tuple3_decode(dec2, dec3, dec4)(p.items[1])
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[T1, T2, T3, T4] = first + prerest
            return rest
        except Exception as e:
            print("Error in tuple4_decode")
            print(e)
            raise e

    return result


assert tuple4_decode(int_decode, int_decode, int_decode, int_decode)(
    tuple4_encode(int_encode, int_encode, int_encode, int_encode)((1, 2, 3, 4))
) == (1, 2, 3, 4)


def tuple5_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
):
    def result(p) -> Tuple[T1, T2, T3, T4, T5]:
        try:
            x1 = p.items[0]
            prerest: Tuple[T2, T3, T4, T5] = tuple4_decode(dec2, dec3, dec4, dec5)(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[T1, T2, T3, T4, T5] = first + prerest
            return rest
        except Exception as e:
            print("Error in tuple5_decode")
            print(e)
            raise e

    return result


assert tuple5_decode(int_decode, int_decode, int_decode, int_decode, int_decode)(
    tuple5_encode(int_encode, int_encode, int_encode, int_encode, int_encode)(
        (1, 2, 3, 4, 5)
    )
) == (1, 2, 3, 4, 5)


def tuple6_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
):
    def result(p) -> Tuple[T1, T2, T3, T4, T5, T6]:
        try:
            x1 = p.items[0]
            prerest: Tuple[T2, T3, T4, T5, T6] = tuple5_decode(
                dec2, dec3, dec4, dec5, dec6
            )(p.items[1])
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[T1, T2, T3, T4, T5, T6] = first + prerest
            return rest
        except Exception as e:
            print("Error in tuple6_decode")
            print(e)
            raise e

    return result


assert tuple6_decode(
    int_decode, int_decode, int_decode, int_decode, int_decode, int_decode
)(
    tuple6_encode(
        int_encode, int_encode, int_encode, int_encode, int_encode, int_encode
    )((1, 2, 3, 4, 5, 6))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
)


def tuple7_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
):
    def result(p) -> Tuple[T1, T2, T3, T4, T5, T6, T7]:
        try:
            x1 = p.items[0]
            prerest: Tuple[T2, T3, T4, T5, T6, T7] = tuple6_decode(
                dec2, dec3, dec4, dec5, dec6, dec7
            )(p.items[1])
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[T1, T2, T3, T4, T5, T6, T7] = first + prerest
            return rest
        except Exception as e:
            print("Error in tuple7_decode")
            print(e)
            raise e

    return result


assert tuple7_decode(
    int_decode, int_decode, int_decode, int_decode, int_decode, int_decode, int_decode
)(
    tuple7_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
)


def tuple8_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
):
    def result(p) -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8]:
        try:
            x1 = p.items[0]
            prerest: Tuple[T2, T3, T4, T5, T6, T7, T8] = tuple7_decode(
                dec2, dec3, dec4, dec5, dec6, dec7, dec8
            )(p.items[1])
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[T1, T2, T3, T4, T5, T6, T7, T8] = first + prerest
            return rest
        except Exception as e:
            print("Error in tuple8_decode")
            print(e)
            raise e

    return result


assert tuple8_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple8_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
)


def tuple9_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
):
    def result(p) -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9]:
        try:
            x1 = p.items[0]
            prerest: Tuple[T2, T3, T4, T5, T6, T7, T8, T9] = tuple8_decode(
                dec2, dec3, dec4, dec5, dec6, dec7, dec8, dec9
            )(p.items[1])
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9] = first + prerest
            return rest
        except Exception as e:
            print("Error in tuple9_decode")
            print(e)
            raise e

    return result


assert tuple9_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple9_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
)


def tuple10_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
):
    def result(p) -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10]:
        try:
            x1 = p.items[0]
            prerest: Tuple[T2, T3, T4, T5, T6, T7, T8, T9, T10] = tuple9_decode(
                dec2, dec3, dec4, dec5, dec6, dec7, dec8, dec9, dec10
            )(p.items[1])
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10] = first + prerest
            return rest
        except Exception as e:
            print("Error in tuple10_decode")
            print(e)
            raise e

    return result


assert tuple10_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple10_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
)


def tuple11_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
):
    def result(p) -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11]:
        try:
            x1 = p.items[0]
            prerest: Tuple[T2, T3, T4, T5, T6, T7, T8, T9, T10, T11] = tuple10_decode(
                dec2, dec3, dec4, dec5, dec6, dec7, dec8, dec9, dec10, dec11
            )(p.items[1])
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11] = first + prerest
            return rest
        except Exception as e:
            print("Error in tuple11_decode")
            print(e)
            raise e

    return result


assert tuple11_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple11_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
)


def tuple12_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
):
    def result(p) -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12
            ] = tuple11_decode(
                dec2, dec3, dec4, dec5, dec6, dec7, dec8, dec9, dec10, dec11, dec12
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12] = (
                first + prerest
            )
            return rest
        except Exception as e:
            print("Error in tuple12_decode")
            print(e)
            raise e

    return result


assert tuple12_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple12_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
)


def tuple13_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
    dec13: Callable[[MichelsonType], T13],
):
    def result(p) -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13
            ] = tuple12_decode(
                dec2,
                dec3,
                dec4,
                dec5,
                dec6,
                dec7,
                dec8,
                dec9,
                dec10,
                dec11,
                dec12,
                dec13,
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13] = (
                first + prerest
            )
            return rest
        except Exception as e:
            print("Error in tuple13_decode")
            print(e)
            raise e

    return result


assert tuple13_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple13_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
)


def tuple14_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
    dec13: Callable[[MichelsonType], T13],
    dec14: Callable[[MichelsonType], T14],
):
    def result(p) -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14
            ] = tuple13_decode(
                dec2,
                dec3,
                dec4,
                dec5,
                dec6,
                dec7,
                dec8,
                dec9,
                dec10,
                dec11,
                dec12,
                dec13,
                dec14,
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14] = (
                first + prerest
            )
            return rest
        except Exception as e:
            print("Error in tuple14_decode")
            print(e)
            raise e

    return result


assert tuple14_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple14_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
)


def tuple15_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
    dec13: Callable[[MichelsonType], T13],
    dec14: Callable[[MichelsonType], T14],
    dec15: Callable[[MichelsonType], T15],
):
    def result(
        p,
    ) -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15
            ] = tuple14_decode(
                dec2,
                dec3,
                dec4,
                dec5,
                dec6,
                dec7,
                dec8,
                dec9,
                dec10,
                dec11,
                dec12,
                dec13,
                dec14,
                dec15,
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[
                T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15
            ] = (first + prerest)
            return rest
        except Exception as e:
            print("Error in tuple15_decode")
            print(e)
            raise e

    return result


assert tuple15_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple15_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
)


def tuple16_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
    dec13: Callable[[MichelsonType], T13],
    dec14: Callable[[MichelsonType], T14],
    dec15: Callable[[MichelsonType], T15],
    dec16: Callable[[MichelsonType], T16],
):
    def result(
        p,
    ) -> Tuple[T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16
            ] = tuple15_decode(
                dec2,
                dec3,
                dec4,
                dec5,
                dec6,
                dec7,
                dec8,
                dec9,
                dec10,
                dec11,
                dec12,
                dec13,
                dec14,
                dec15,
                dec16,
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[
                T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16
            ] = (first + prerest)
            return rest
        except Exception as e:
            print("Error in tuple16_decode")
            print(e)
            raise e

    return result


assert tuple16_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple16_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
)


def tuple17_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
    dec13: Callable[[MichelsonType], T13],
    dec14: Callable[[MichelsonType], T14],
    dec15: Callable[[MichelsonType], T15],
    dec16: Callable[[MichelsonType], T16],
    dec17: Callable[[MichelsonType], T17],
):
    def result(
        p,
    ) -> Tuple[
        T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17
    ]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17
            ] = tuple16_decode(
                dec2,
                dec3,
                dec4,
                dec5,
                dec6,
                dec7,
                dec8,
                dec9,
                dec10,
                dec11,
                dec12,
                dec13,
                dec14,
                dec15,
                dec16,
                dec17,
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[
                T1,
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
            ] = (
                first + prerest
            )
            return rest
        except Exception as e:
            print("Error in tuple17_decode")
            print(e)
            raise e

    return result


assert tuple17_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple17_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
)


def tuple18_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
    dec13: Callable[[MichelsonType], T13],
    dec14: Callable[[MichelsonType], T14],
    dec15: Callable[[MichelsonType], T15],
    dec16: Callable[[MichelsonType], T16],
    dec17: Callable[[MichelsonType], T17],
    dec18: Callable[[MichelsonType], T18],
):
    def result(
        p,
    ) -> Tuple[
        T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18
    ]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
            ] = tuple17_decode(
                dec2,
                dec3,
                dec4,
                dec5,
                dec6,
                dec7,
                dec8,
                dec9,
                dec10,
                dec11,
                dec12,
                dec13,
                dec14,
                dec15,
                dec16,
                dec17,
                dec18,
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[
                T1,
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
            ] = (
                first + prerest
            )
            return rest
        except Exception as e:
            print("Error in tuple18_decode")
            print(e)
            raise e

    return result


assert tuple18_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple18_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
)


def tuple19_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
    dec13: Callable[[MichelsonType], T13],
    dec14: Callable[[MichelsonType], T14],
    dec15: Callable[[MichelsonType], T15],
    dec16: Callable[[MichelsonType], T16],
    dec17: Callable[[MichelsonType], T17],
    dec18: Callable[[MichelsonType], T18],
    dec19: Callable[[MichelsonType], T19],
):
    def result(
        p,
    ) -> Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
    ]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
            ] = tuple18_decode(
                dec2,
                dec3,
                dec4,
                dec5,
                dec6,
                dec7,
                dec8,
                dec9,
                dec10,
                dec11,
                dec12,
                dec13,
                dec14,
                dec15,
                dec16,
                dec17,
                dec18,
                dec19,
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[
                T1,
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
            ] = (
                first + prerest
            )
            return rest
        except Exception as e:
            print("Error in tuple19_decode")
            print(e)
            raise e

    return result


assert tuple19_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple19_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
)


def tuple20_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
    dec13: Callable[[MichelsonType], T13],
    dec14: Callable[[MichelsonType], T14],
    dec15: Callable[[MichelsonType], T15],
    dec16: Callable[[MichelsonType], T16],
    dec17: Callable[[MichelsonType], T17],
    dec18: Callable[[MichelsonType], T18],
    dec19: Callable[[MichelsonType], T19],
    dec20: Callable[[MichelsonType], T20],
):
    def result(
        p,
    ) -> Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
    ]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
                T20,
            ] = tuple19_decode(
                dec2,
                dec3,
                dec4,
                dec5,
                dec6,
                dec7,
                dec8,
                dec9,
                dec10,
                dec11,
                dec12,
                dec13,
                dec14,
                dec15,
                dec16,
                dec17,
                dec18,
                dec19,
                dec20,
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[
                T1,
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
                T20,
            ] = (
                first + prerest
            )
            return rest
        except Exception as e:
            print("Error in tuple20_decode")
            print(e)
            raise e

    return result


assert tuple20_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple20_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
)


def tuple21_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
    dec13: Callable[[MichelsonType], T13],
    dec14: Callable[[MichelsonType], T14],
    dec15: Callable[[MichelsonType], T15],
    dec16: Callable[[MichelsonType], T16],
    dec17: Callable[[MichelsonType], T17],
    dec18: Callable[[MichelsonType], T18],
    dec19: Callable[[MichelsonType], T19],
    dec20: Callable[[MichelsonType], T20],
    dec21: Callable[[MichelsonType], T21],
):
    def result(
        p,
    ) -> Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
        T21,
    ]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
                T20,
                T21,
            ] = tuple20_decode(
                dec2,
                dec3,
                dec4,
                dec5,
                dec6,
                dec7,
                dec8,
                dec9,
                dec10,
                dec11,
                dec12,
                dec13,
                dec14,
                dec15,
                dec16,
                dec17,
                dec18,
                dec19,
                dec20,
                dec21,
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[
                T1,
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
                T20,
                T21,
            ] = (
                first + prerest
            )
            return rest
        except Exception as e:
            print("Error in tuple21_decode")
            print(e)
            raise e

    return result


assert tuple21_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple21_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
)


def tuple22_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
    dec13: Callable[[MichelsonType], T13],
    dec14: Callable[[MichelsonType], T14],
    dec15: Callable[[MichelsonType], T15],
    dec16: Callable[[MichelsonType], T16],
    dec17: Callable[[MichelsonType], T17],
    dec18: Callable[[MichelsonType], T18],
    dec19: Callable[[MichelsonType], T19],
    dec20: Callable[[MichelsonType], T20],
    dec21: Callable[[MichelsonType], T21],
    dec22: Callable[[MichelsonType], T22],
):
    def result(
        p,
    ) -> Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
        T21,
        T22,
    ]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
                T20,
                T21,
                T22,
            ] = tuple21_decode(
                dec2,
                dec3,
                dec4,
                dec5,
                dec6,
                dec7,
                dec8,
                dec9,
                dec10,
                dec11,
                dec12,
                dec13,
                dec14,
                dec15,
                dec16,
                dec17,
                dec18,
                dec19,
                dec20,
                dec21,
                dec22,
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[
                T1,
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
                T20,
                T21,
                T22,
            ] = (
                first + prerest
            )
            return rest
        except Exception as e:
            print("Error in tuple22_decode")
            print(e)
            raise e

    return result


assert tuple22_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple22_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22))
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
)


def tuple23_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
    dec13: Callable[[MichelsonType], T13],
    dec14: Callable[[MichelsonType], T14],
    dec15: Callable[[MichelsonType], T15],
    dec16: Callable[[MichelsonType], T16],
    dec17: Callable[[MichelsonType], T17],
    dec18: Callable[[MichelsonType], T18],
    dec19: Callable[[MichelsonType], T19],
    dec20: Callable[[MichelsonType], T20],
    dec21: Callable[[MichelsonType], T21],
    dec22: Callable[[MichelsonType], T22],
    dec23: Callable[[MichelsonType], T23],
):
    def result(
        p,
    ) -> Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
        T21,
        T22,
        T23,
    ]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
                T20,
                T21,
                T22,
                T23,
            ] = tuple22_decode(
                dec2,
                dec3,
                dec4,
                dec5,
                dec6,
                dec7,
                dec8,
                dec9,
                dec10,
                dec11,
                dec12,
                dec13,
                dec14,
                dec15,
                dec16,
                dec17,
                dec18,
                dec19,
                dec20,
                dec21,
                dec22,
                dec23,
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[
                T1,
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
                T20,
                T21,
                T22,
                T23,
            ] = (
                first + prerest
            )
            return rest
        except Exception as e:
            print("Error in tuple23_decode")
            print(e)
            raise e

    return result


assert tuple23_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple23_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )(
        (
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
        )
    )
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
)


def tuple24_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
    dec13: Callable[[MichelsonType], T13],
    dec14: Callable[[MichelsonType], T14],
    dec15: Callable[[MichelsonType], T15],
    dec16: Callable[[MichelsonType], T16],
    dec17: Callable[[MichelsonType], T17],
    dec18: Callable[[MichelsonType], T18],
    dec19: Callable[[MichelsonType], T19],
    dec20: Callable[[MichelsonType], T20],
    dec21: Callable[[MichelsonType], T21],
    dec22: Callable[[MichelsonType], T22],
    dec23: Callable[[MichelsonType], T23],
    dec24: Callable[[MichelsonType], T24],
):
    def result(
        p,
    ) -> Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
        T21,
        T22,
        T23,
        T24,
    ]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
                T20,
                T21,
                T22,
                T23,
                T24,
            ] = tuple23_decode(
                dec2,
                dec3,
                dec4,
                dec5,
                dec6,
                dec7,
                dec8,
                dec9,
                dec10,
                dec11,
                dec12,
                dec13,
                dec14,
                dec15,
                dec16,
                dec17,
                dec18,
                dec19,
                dec20,
                dec21,
                dec22,
                dec23,
                dec24,
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[
                T1,
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
                T20,
                T21,
                T22,
                T23,
                T24,
            ] = (
                first + prerest
            )
            return rest
        except Exception as e:
            print("Error in tuple24_decode")
            print(e)
            raise e

    return result


assert tuple24_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple24_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )(
        (
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
        )
    )
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
)


def tuple25_decode(
    dec1: Callable[[MichelsonType], T1],
    dec2: Callable[[MichelsonType], T2],
    dec3: Callable[[MichelsonType], T3],
    dec4: Callable[[MichelsonType], T4],
    dec5: Callable[[MichelsonType], T5],
    dec6: Callable[[MichelsonType], T6],
    dec7: Callable[[MichelsonType], T7],
    dec8: Callable[[MichelsonType], T8],
    dec9: Callable[[MichelsonType], T9],
    dec10: Callable[[MichelsonType], T10],
    dec11: Callable[[MichelsonType], T11],
    dec12: Callable[[MichelsonType], T12],
    dec13: Callable[[MichelsonType], T13],
    dec14: Callable[[MichelsonType], T14],
    dec15: Callable[[MichelsonType], T15],
    dec16: Callable[[MichelsonType], T16],
    dec17: Callable[[MichelsonType], T17],
    dec18: Callable[[MichelsonType], T18],
    dec19: Callable[[MichelsonType], T19],
    dec20: Callable[[MichelsonType], T20],
    dec21: Callable[[MichelsonType], T21],
    dec22: Callable[[MichelsonType], T22],
    dec23: Callable[[MichelsonType], T23],
    dec24: Callable[[MichelsonType], T24],
    dec25: Callable[[MichelsonType], T25],
):
    def result(
        p,
    ) -> Tuple[
        T1,
        T2,
        T3,
        T4,
        T5,
        T6,
        T7,
        T8,
        T9,
        T10,
        T11,
        T12,
        T13,
        T14,
        T15,
        T16,
        T17,
        T18,
        T19,
        T20,
        T21,
        T22,
        T23,
        T24,
        T25,
    ]:
        try:
            x1 = p.items[0]
            prerest: Tuple[
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
                T20,
                T21,
                T22,
                T23,
                T24,
                T25,
            ] = tuple24_decode(
                dec2,
                dec3,
                dec4,
                dec5,
                dec6,
                dec7,
                dec8,
                dec9,
                dec10,
                dec11,
                dec12,
                dec13,
                dec14,
                dec15,
                dec16,
                dec17,
                dec18,
                dec19,
                dec20,
                dec21,
                dec22,
                dec23,
                dec24,
                dec25,
            )(
                p.items[1]
            )
            first: Tuple[T1] = (dec1(x1),)
            rest: Tuple[
                T1,
                T2,
                T3,
                T4,
                T5,
                T6,
                T7,
                T8,
                T9,
                T10,
                T11,
                T12,
                T13,
                T14,
                T15,
                T16,
                T17,
                T18,
                T19,
                T20,
                T21,
                T22,
                T23,
                T24,
                T25,
            ] = (
                first + prerest
            )
            return rest
        except Exception as e:
            print("Error in tuple25_decode")
            print(e)
            raise e

    return result


assert tuple25_decode(
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
    int_decode,
)(
    tuple25_encode(
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
        int_encode,
    )(
        (
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
        )
    )
) == (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
)
