from typing import TypedDict
from pytezos import pytezos
from pytezos import ContractInterface
from pytezos import micheline_to_michelson
from pytezos.operation.group import OperationGroup
import json
import hashlib
import base58


class identity(TypedDict):
    sk: str
    pk: str
    pkh: str


NODE_URLS = {
    "sandbox_node": "http://127.0.0.1:18731",
    "flextesa_node": "http://localhost:20000",
    "ithaca_node": "https://ithacanet.tezos.marigold.dev",
    "ithacanet_node": "https://ithacanet.tezos.marigold.dev",
    "ghostnet_node": "https://ghostnet.ecadinfra.com",
    "jakarta_node": "https://jakartanet.ecadinfra.com",
    "lima_node": "https://limanet.ecadinfra.com",
    "jakartanet_node": "https://jakartanet.ecadinfra.com",
    "mainnet_node": "https://tz.functori.com",
    "default_node": "http://localhost:20000",
}


def get_node(network):
    return NODE_URLS.get(network + "_node", NODE_URLS["default_node"])


alice: identity = {
    "sk": "edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq",
    "pk": "edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn",
    "pkh": "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb",
}
bob: identity = {
    "sk": "edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt",
    "pk": "edpkurPsQ8eUApnLUJ9ZPDvu98E8VNj4KtJa1aZr16Cr5ow5VHKnz4",
    "pkh": "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6",
}


def originated_contract_hash(opg_hash, index):
    opg_hash = base58.b58decode_check(opg_hash)
    opg_hash = opg_hash[2:]
    index_bytes = index.to_bytes(4, "big")
    msg = opg_hash + index_bytes
    digest = hashlib.blake2b(msg, digest_size=20).digest()
    prefix = prefix = bytearray([2, 90, 121])
    return base58.b58encode_check(prefix + digest).decode("utf-8")


def compute_kt1(opg_hash) -> str:
    return originated_contract_hash(opg_hash, 0)


def callEntrypoint(
    _from: identity, kt1: str, entrypoint: str, param, amount, network, debug=False
) -> str:
    tezos = pytezos.using(shell=get_node(network), key=_from["sk"])
    if debug:
        print("param" + str(param))
    op = tezos.transaction(
        source=_from["sk"],
        destination=kt1,
        parameters={"entrypoint": entrypoint, "value": param},
        amount=amount,
    )
    opg = tezos.bulk(op).fill().sign().inject(_async=False)
    try:
        op_hash: str = opg["hash"]
    except Exception as e:
        raise ValueError("Operation could not go through")
    return op_hash


def deployContract(
    codeFileName: str, _from: identity, initial_storage, amount, network, debug=False
) -> str | None:
    with open(codeFileName, "r") as file:
        micheline_code = json.load(file)
        code = micheline_to_michelson(micheline_code)
    contract: ContractInterface = ContractInterface.from_michelson(code)
    if debug:
        print("network is:" + network)
        print("node is:" + get_node(network))
    tezos = pytezos.using(shell=get_node(network), key=_from["sk"])
    context = tezos.context
    opg = OperationGroup(context).origination(
        script={"code": contract.code, "storage": initial_storage}
    )
    opg = tezos.bulk(opg).fill().sign().inject(_async=False)
    op_hash = opg["hash"]
    kt1 = compute_kt1(op_hash)
    return kt1
