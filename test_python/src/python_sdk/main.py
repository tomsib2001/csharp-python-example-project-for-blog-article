from importlib.metadata import metadata
import time
import fa2_python_interface
import factori_types
import blockchain


def main():
    debug = False
    initial_storage = fa2_python_interface.initial_blockchain_storage
    initial_storage["administrator"] = blockchain.alice["pkh"]
    kt1 = fa2_python_interface.deploy(
        _from=blockchain.alice,
        initial_storage=initial_storage,
        amount=0,
        network="ghostnet",
        debug=debug,
    )
    print("Kt1: " + str(kt1))
    time.sleep(15)  # we need to wait for one block before calling the contract
    mint_param: fa2_python_interface.Mint = {
        "amount": 1000,
        "address": blockchain.alice["pkh"],
        "metadata": factori_types.Map(),
        "token_id": 0,
    }
    ophash = fa2_python_interface.callMint(
        kt1,
        _from=blockchain.alice,
        param=mint_param,
        networkName="ghostnet",
        debug=debug,
    )
    print("operation hash: " + ophash)


main()
