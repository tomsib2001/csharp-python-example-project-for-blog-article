from pytezos import pytezos

# from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

Token_id = factori_types.nat


def token_id_generator() -> Token_id:
    return factori_types.nat_generator()


def token_id_encode(arg: Token_id):
    try:
        return factori_types.nat_encode(arg)
    except Exception as e:
        print("Error in token_id_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def token_id_decode(arg) -> Token_id:
    try:
        return factori_types.nat_decode(arg)
    except Exception as e:
        print(
            "Error in token_id_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_token_id():
    temp = token_id_generator()
    temp_encoded = token_id_encode(temp)
    try:
        print("++++ token_id: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of token_id: " + str(temp))
    temp1 = token_id_decode(temp_encoded)
    if temp1 == temp:
        print("token_id check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "token_id check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_token_id()
# except Exception as e:
# 	print('Error in assert_identity_token_id' + str(e))


Constructor_operator = factori_types.address


def _operator_generator() -> Constructor_operator:
    return factori_types.address_generator()


def _operator_encode(arg: Constructor_operator):
    try:
        return factori_types.address_encode(arg)
    except Exception as e:
        print("Error in _operator_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def _operator_decode(arg) -> Constructor_operator:
    try:
        return factori_types.address_decode(arg)
    except Exception as e:
        print(
            "Error in _operator_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity__operator():
    temp = _operator_generator()
    temp_encoded = _operator_encode(temp)
    try:
        print("++++ _operator: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of _operator: " + str(temp))
    temp1 = _operator_decode(temp_encoded)
    if temp1 == temp:
        print("_operator check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "_operator check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity__operator()
# except Exception as e:
# 	print('Error in assert_identity__operator' + str(e))


class Add_operator(TypedDict):
    owner: Constructor_operator
    _operator: Constructor_operator
    token_id: Token_id


def add_operator_generator() -> Add_operator:
    return {
        "owner": _operator_generator(),
        "_operator": _operator_generator(),
        "token_id": token_id_generator(),
    }


def add_operator_encode(arg: Add_operator):
    try:
        return PairType.from_comb(
            [
                (_operator_encode(arg["owner"])),
                (_operator_encode(arg["_operator"])),
                (token_id_encode(arg["token_id"])),
            ]
        )
    except Exception as e:
        print(
            "Error in add_operator_encode, with input:" + str(arg) + "Error:" + str(e)
        )
        raise e


def add_operator_decode(arg) -> Add_operator:
    try:
        before_projection = (
            factori_types.tuple3_decode(
                _operator_decode, _operator_decode, token_id_decode
            )
        )(arg)
        return {
            "owner": (before_projection[0]),
            "_operator": (before_projection[1]),
            "token_id": (before_projection[2]),
        }

    except Exception as e:
        print(
            "Error in add_operator_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_add_operator():
    temp = add_operator_generator()
    temp_encoded = add_operator_encode(temp)
    try:
        print("++++ add_operator: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of add_operator: " + str(temp))
    temp1 = add_operator_decode(temp_encoded)
    if temp1 == temp:
        print("add_operator check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "add_operator check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_add_operator()
# except Exception as e:
# 	print('Error in assert_identity_add_operator' + str(e))


class Type0_Remove_operator_constructor_subtype(TypedDict):
    kind: str  # "Remove_operator_constructor"
    Remove_operator_element: Add_operator


class Type0_Add_operator_constructor_subtype(TypedDict):
    kind: str  # "Add_operator_constructor"
    Add_operator_element: Add_operator


Type0 = Union[
    Type0_Remove_operator_constructor_subtype, Type0_Add_operator_constructor_subtype
]


def type0_generator() -> Type0:
    l: list[Type0] = [
        (
            {
                "kind": "Add_operator_constructor",
                "Add_operator_element": (add_operator_generator()),
            }
        ),
        (
            {
                "kind": "Remove_operator_constructor",
                "Remove_operator_element": (add_operator_generator()),
            }
        ),
    ]
    return factori_types.chooseFrom(l)


def type0_encode(arg) -> OrType:
    try:
        if arg["kind"] == "Add_operator_constructor":
            return factori_types.make_left(
                add_operator_encode(arg["Add_operator_element"])
            )

        if arg["kind"] == "Remove_operator_constructor":
            return factori_types.make_right(
                add_operator_encode(arg["Remove_operator_element"])
            )
        else:
            raise (ValueError("Unknown kind"))

    except Exception as e:
        print("Error in type0_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def type0_decode(arg: MichelsonType) -> Type0:
    try:
        if arg.is_left():
            return {
                "kind": "Add_operator_constructor",
                "Add_operator_element": add_operator_decode(arg.items[0]),
            }

        if arg.is_right():
            return {
                "kind": "Remove_operator_constructor",
                "Remove_operator_element": add_operator_decode(arg.items[1]),
            }

        raise (ValueError("Value is neither left nor right"))
    except Exception as e:
        print(
            "Error in type0_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_type0():
    temp = type0_generator()
    temp_encoded = type0_encode(temp)
    try:
        print("++++ type0: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of type0: " + str(temp))
    temp1 = type0_decode(temp_encoded)
    if temp1 == temp:
        print("type0 check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "type0 check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_type0()
# except Exception as e:
# 	print('Error in assert_identity_type0' + str(e))


Update_operators = List[Type0]


def update_operators_generator() -> Update_operators:
    return (factori_types.list_generator(type0_generator))()


def update_operators_encode(arg: Update_operators):
    try:
        return factori_types.list_encode(type0_encode)(arg)
    except Exception as e:
        print(
            "Error in update_operators_encode, with input:"
            + str(arg)
            + "Error:"
            + str(e)
        )
        raise e


def update_operators_decode(arg) -> Update_operators:
    try:
        return factori_types.list_decode(type0_decode)(arg)
    except Exception as e:
        print(
            "Error in update_operators_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_update_operators():
    temp = update_operators_generator()
    temp_encoded = update_operators_encode(temp)
    try:
        print("++++ update_operators: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of update_operators: " + str(temp))
    temp1 = update_operators_decode(temp_encoded)
    if temp1 == temp:
        print("update_operators check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "update_operators check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_update_operators()
# except Exception as e:
# 	print('Error in assert_identity_update_operators' + str(e))


def callUpdate_operators(
    kt1: str,
    _from: blockchain.identity,
    param: Update_operators,
    networkName="ghostnet",
    debug=False,
):
    input = update_operators_encode(param)
    ophash = blockchain.callEntrypoint(
        _from,
        kt1,
        "update_operators",
        input.to_micheline_value(),
        0,
        network=networkName,
        debug=debug,
    )
    return ophash


class Type1(TypedDict):
    to_: Constructor_operator
    token_id: Token_id
    amount: Token_id


def type1_generator() -> Type1:
    return {
        "to_": _operator_generator(),
        "token_id": token_id_generator(),
        "amount": token_id_generator(),
    }


def type1_encode(arg: Type1):
    try:
        return PairType.from_comb(
            [
                (_operator_encode(arg["to_"])),
                (token_id_encode(arg["token_id"])),
                (token_id_encode(arg["amount"])),
            ]
        )
    except Exception as e:
        print("Error in type1_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def type1_decode(arg) -> Type1:
    try:
        before_projection = (
            factori_types.tuple3_decode(
                _operator_decode, token_id_decode, token_id_decode
            )
        )(arg)
        return {
            "to_": (before_projection[0]),
            "token_id": (before_projection[1]),
            "amount": (before_projection[2]),
        }

    except Exception as e:
        print(
            "Error in type1_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_type1():
    temp = type1_generator()
    temp_encoded = type1_encode(temp)
    try:
        print("++++ type1: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of type1: " + str(temp))
    temp1 = type1_decode(temp_encoded)
    if temp1 == temp:
        print("type1 check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "type1 check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_type1()
# except Exception as e:
# 	print('Error in assert_identity_type1' + str(e))


class Type2(TypedDict):
    from_: Constructor_operator
    txs: List[Type1]


def type2_generator() -> Type2:
    return {
        "from_": _operator_generator(),
        "txs": (factori_types.list_generator(type1_generator))(),
    }


def type2_encode(arg: Type2):
    try:
        return PairType.from_comb(
            [
                (_operator_encode(arg["from_"])),
                (factori_types.list_encode(type1_encode)(arg["txs"])),
            ]
        )
    except Exception as e:
        print("Error in type2_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def type2_decode(arg) -> Type2:
    try:
        before_projection = (
            factori_types.tuple2_decode(
                _operator_decode, factori_types.list_decode(type1_decode)
            )
        )(arg)
        return {"from_": (before_projection[0]), "txs": (before_projection[1])}

    except Exception as e:
        print(
            "Error in type2_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_type2():
    temp = type2_generator()
    temp_encoded = type2_encode(temp)
    try:
        print("++++ type2: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of type2: " + str(temp))
    temp1 = type2_decode(temp_encoded)
    if temp1 == temp:
        print("type2 check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "type2 check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_type2()
# except Exception as e:
# 	print('Error in assert_identity_type2' + str(e))


Transfer = List[Type2]


def transfer_generator() -> Transfer:
    return (factori_types.list_generator(type2_generator))()


def transfer_encode(arg: Transfer):
    try:
        return factori_types.list_encode(type2_encode)(arg)
    except Exception as e:
        print("Error in transfer_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def transfer_decode(arg) -> Transfer:
    try:
        return factori_types.list_decode(type2_decode)(arg)
    except Exception as e:
        print(
            "Error in transfer_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_transfer():
    temp = transfer_generator()
    temp_encoded = transfer_encode(temp)
    try:
        print("++++ transfer: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of transfer: " + str(temp))
    temp1 = transfer_decode(temp_encoded)
    if temp1 == temp:
        print("transfer check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "transfer check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_transfer()
# except Exception as e:
# 	print('Error in assert_identity_transfer' + str(e))


def callTransfer(
    kt1: str,
    _from: blockchain.identity,
    param: Transfer,
    networkName="ghostnet",
    debug=False,
):
    input = transfer_encode(param)
    ophash = blockchain.callEntrypoint(
        _from,
        kt1,
        "transfer",
        input.to_micheline_value(),
        0,
        network=networkName,
        debug=debug,
    )
    return ophash


Set_pause = bool


def set_pause_generator() -> Set_pause:
    return factori_types.bool_generator()


def set_pause_encode(arg: Set_pause):
    try:
        return factori_types.bool_encode(arg)
    except Exception as e:
        print("Error in set_pause_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def set_pause_decode(arg) -> Set_pause:
    try:
        return factori_types.bool_decode(arg)
    except Exception as e:
        print(
            "Error in set_pause_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_set_pause():
    temp = set_pause_generator()
    temp_encoded = set_pause_encode(temp)
    try:
        print("++++ set_pause: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of set_pause: " + str(temp))
    temp1 = set_pause_decode(temp_encoded)
    if temp1 == temp:
        print("set_pause check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "set_pause check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_set_pause()
# except Exception as e:
# 	print('Error in assert_identity_set_pause' + str(e))


def callSet_pause(
    kt1: str,
    _from: blockchain.identity,
    param: Set_pause,
    networkName="ghostnet",
    debug=False,
):
    input = set_pause_encode(param)
    ophash = blockchain.callEntrypoint(
        _from,
        kt1,
        "set_pause",
        input.to_micheline_value(),
        0,
        network=networkName,
        debug=debug,
    )
    return ophash


V = bytes


def v_generator() -> V:
    return factori_types.bytes_generator()


def v_encode(arg: V):
    try:
        return factori_types.bytes_encode(arg)
    except Exception as e:
        print("Error in v_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def v_decode(arg) -> V:
    try:
        return factori_types.bytes_decode(arg)
    except Exception as e:
        print(
            "Error in v_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_v():
    temp = v_generator()
    temp_encoded = v_encode(temp)
    try:
        print("++++ v: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of v: " + str(temp))
    temp1 = v_decode(temp_encoded)
    if temp1 == temp:
        print("v check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "v check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_v()
# except Exception as e:
# 	print('Error in assert_identity_v' + str(e))


K = str


def k_generator() -> K:
    return factori_types.string_generator()


def k_encode(arg: K):
    try:
        return factori_types.string_encode(arg)
    except Exception as e:
        print("Error in k_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def k_decode(arg) -> K:
    try:
        return factori_types.string_decode(arg)
    except Exception as e:
        print(
            "Error in k_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_k():
    temp = k_generator()
    temp_encoded = k_encode(temp)
    try:
        print("++++ k: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of k: " + str(temp))
    temp1 = k_decode(temp_encoded)
    if temp1 == temp:
        print("k check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "k check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_k()
# except Exception as e:
# 	print('Error in assert_identity_k' + str(e))


class Set_metadata(TypedDict):
    k: K
    v: V


def set_metadata_generator() -> Set_metadata:
    return {"k": k_generator(), "v": v_generator()}


def set_metadata_encode(arg: Set_metadata):
    try:
        return PairType.from_comb([(k_encode(arg["k"])), (v_encode(arg["v"]))])
    except Exception as e:
        print(
            "Error in set_metadata_encode, with input:" + str(arg) + "Error:" + str(e)
        )
        raise e


def set_metadata_decode(arg) -> Set_metadata:
    try:
        before_projection = (factori_types.tuple2_decode(k_decode, v_decode))(arg)
        return {"k": (before_projection[0]), "v": (before_projection[1])}

    except Exception as e:
        print(
            "Error in set_metadata_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_set_metadata():
    temp = set_metadata_generator()
    temp_encoded = set_metadata_encode(temp)
    try:
        print("++++ set_metadata: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of set_metadata: " + str(temp))
    temp1 = set_metadata_decode(temp_encoded)
    if temp1 == temp:
        print("set_metadata check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "set_metadata check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_set_metadata()
# except Exception as e:
# 	print('Error in assert_identity_set_metadata' + str(e))


def callSet_metadata(
    kt1: str,
    _from: blockchain.identity,
    param: Set_metadata,
    networkName="ghostnet",
    debug=False,
):
    input = set_metadata_encode(param)
    ophash = blockchain.callEntrypoint(
        _from,
        kt1,
        "set_metadata",
        input.to_micheline_value(),
        0,
        network=networkName,
        debug=debug,
    )
    return ophash


Set_administrator = Constructor_operator


def set_administrator_generator() -> Set_administrator:
    return _operator_generator()


def set_administrator_encode(arg: Set_administrator):
    try:
        return _operator_encode(arg)
    except Exception as e:
        print(
            "Error in set_administrator_encode, with input:"
            + str(arg)
            + "Error:"
            + str(e)
        )
        raise e


def set_administrator_decode(arg) -> Set_administrator:
    try:
        return _operator_decode(arg)
    except Exception as e:
        print(
            "Error in set_administrator_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_set_administrator():
    temp = set_administrator_generator()
    temp_encoded = set_administrator_encode(temp)
    try:
        print("++++ set_administrator: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of set_administrator: " + str(temp))
    temp1 = set_administrator_decode(temp_encoded)
    if temp1 == temp:
        print("set_administrator check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "set_administrator check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_set_administrator()
# except Exception as e:
# 	print('Error in assert_identity_set_administrator' + str(e))


def callSet_administrator(
    kt1: str,
    _from: blockchain.identity,
    param: Set_administrator,
    networkName="ghostnet",
    debug=False,
):
    input = set_administrator_encode(param)
    ophash = blockchain.callEntrypoint(
        _from,
        kt1,
        "set_administrator",
        input.to_micheline_value(),
        0,
        network=networkName,
        debug=debug,
    )
    return ophash


class Mint(TypedDict):
    amount: Token_id
    address: Constructor_operator
    metadata: factori_types.Map
    token_id: Token_id


def mint_generator() -> Mint:
    return {
        "amount": token_id_generator(),
        "address": _operator_generator(),
        "metadata": (factori_types.map_generator(k_generator, v_generator))(),
        "token_id": token_id_generator(),
    }


def mint_encode(arg: Mint):
    try:
        return PairType.from_comb(
            [
                PairType.from_comb(
                    [
                        (_operator_encode(arg["address"])),
                        (token_id_encode(arg["amount"])),
                    ]
                ),
                (factori_types.map_encode(k_encode, v_encode)(arg["metadata"])),
                (token_id_encode(arg["token_id"])),
            ]
        )
    except Exception as e:
        print("Error in mint_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def mint_decode(arg) -> Mint:
    try:
        before_projection = (
            factori_types.tuple3_decode(
                (factori_types.tuple2_decode(_operator_decode, token_id_decode)),
                factori_types.map_decode(k_decode, v_decode),
                token_id_decode,
            )
        )(arg)
        return {
            "address": (before_projection[0][0]),
            "amount": (before_projection[0][1]),
            "metadata": (before_projection[1]),
            "token_id": (before_projection[2]),
        }

    except Exception as e:
        print(
            "Error in mint_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_mint():
    temp = mint_generator()
    temp_encoded = mint_encode(temp)
    try:
        print("++++ mint: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of mint: " + str(temp))
    temp1 = mint_decode(temp_encoded)
    if temp1 == temp:
        print("mint check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "mint check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_mint()
# except Exception as e:
# 	print('Error in assert_identity_mint' + str(e))


def callMint(
    kt1: str,
    _from: blockchain.identity,
    param: Mint,
    networkName="ghostnet",
    debug=False,
):
    input = mint_encode(param)
    ophash = blockchain.callEntrypoint(
        _from,
        kt1,
        "mint",
        input.to_micheline_value(),
        0,
        network=networkName,
        debug=debug,
    )
    return ophash


class Type6(TypedDict):
    owner: Constructor_operator
    token_id: Token_id


def type6_generator() -> Type6:
    return {"owner": _operator_generator(), "token_id": token_id_generator()}


def type6_encode(arg: Type6):
    try:
        return PairType.from_comb(
            [(_operator_encode(arg["owner"])), (token_id_encode(arg["token_id"]))]
        )
    except Exception as e:
        print("Error in type6_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def type6_decode(arg) -> Type6:
    try:
        before_projection = (
            factori_types.tuple2_decode(_operator_decode, token_id_decode)
        )(arg)
        return {"owner": (before_projection[0]), "token_id": (before_projection[1])}

    except Exception as e:
        print(
            "Error in type6_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_type6():
    temp = type6_generator()
    temp_encoded = type6_encode(temp)
    try:
        print("++++ type6: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of type6: " + str(temp))
    temp1 = type6_decode(temp_encoded)
    if temp1 == temp:
        print("type6 check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "type6 check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_type6()
# except Exception as e:
# 	print('Error in assert_identity_type6' + str(e))


class Balance_of(TypedDict):
    requests: List[Type6]
    callback: factori_types.contract


def balance_of_generator() -> Balance_of:
    return {
        "requests": (factori_types.list_generator(type6_generator))(),
        "callback": factori_types.contract_generator(),
    }


def balance_of_encode(arg: Balance_of):
    try:
        return PairType.from_comb(
            [
                (factori_types.list_encode(type6_encode)(arg["requests"])),
                (factori_types.contract_encode(arg["callback"])),
            ]
        )
    except Exception as e:
        print("Error in balance_of_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def balance_of_decode(arg) -> Balance_of:
    try:
        before_projection = (
            factori_types.tuple2_decode(
                factori_types.list_decode(type6_decode), factori_types.contract_decode
            )
        )(arg)
        return {"requests": (before_projection[0]), "callback": (before_projection[1])}

    except Exception as e:
        print(
            "Error in balance_of_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_balance_of():
    temp = balance_of_generator()
    temp_encoded = balance_of_encode(temp)
    try:
        print("++++ balance_of: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of balance_of: " + str(temp))
    temp1 = balance_of_decode(temp_encoded)
    if temp1 == temp:
        print("balance_of check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "balance_of check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_balance_of()
# except Exception as e:
# 	print('Error in assert_identity_balance_of' + str(e))


def callBalance_of(
    kt1: str,
    _from: blockchain.identity,
    param: Balance_of,
    networkName="ghostnet",
    debug=False,
):
    input = balance_of_encode(param)
    ophash = blockchain.callEntrypoint(
        _from,
        kt1,
        "balance_of",
        input.to_micheline_value(),
        0,
        network=networkName,
        debug=debug,
    )
    return ophash


class Type8(TypedDict):
    token_id: Token_id
    token_info: factori_types.Map


def type8_generator() -> Type8:
    return {
        "token_id": token_id_generator(),
        "token_info": (factori_types.map_generator(k_generator, v_generator))(),
    }


def type8_encode(arg: Type8):
    try:
        return PairType.from_comb(
            [
                (token_id_encode(arg["token_id"])),
                (factori_types.map_encode(k_encode, v_encode)(arg["token_info"])),
            ]
        )
    except Exception as e:
        print("Error in type8_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def type8_decode(arg) -> Type8:
    try:
        before_projection = (
            factori_types.tuple2_decode(
                token_id_decode, factori_types.map_decode(k_decode, v_decode)
            )
        )(arg)
        return {
            "token_id": (before_projection[0]),
            "token_info": (before_projection[1]),
        }

    except Exception as e:
        print(
            "Error in type8_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_type8():
    temp = type8_generator()
    temp_encoded = type8_encode(temp)
    try:
        print("++++ type8: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of type8: " + str(temp))
    temp1 = type8_decode(temp_encoded)
    if temp1 == temp:
        print("type8 check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "type8 check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_type8()
# except Exception as e:
# 	print('Error in assert_identity_type8' + str(e))


class Storage(TypedDict):
    paused: Set_pause
    operators: factori_types.BigMap
    all_tokens: Token_id
    administrator: Constructor_operator
    ledger: factori_types.BigMap
    metadata: factori_types.BigMap
    token_metadata: factori_types.BigMap
    total_supply: factori_types.BigMap


def storage_generator() -> Storage:
    return {
        "paused": set_pause_generator(),
        "operators": (
            factori_types.big_map_generator(
                add_operator_generator, factori_types.unit_generator
            )
        )(),
        "all_tokens": token_id_generator(),
        "administrator": _operator_generator(),
        "ledger": (
            factori_types.big_map_generator(
                (
                    factori_types.tuple2_generator(
                        _operator_generator, token_id_generator
                    )
                ),
                token_id_generator,
            )
        )(),
        "metadata": (factori_types.big_map_generator(k_generator, v_generator))(),
        "token_metadata": (
            factori_types.big_map_generator(token_id_generator, type8_generator)
        )(),
        "total_supply": (
            factori_types.big_map_generator(token_id_generator, token_id_generator)
        )(),
    }


def storage_encode(arg: Storage):
    try:
        return PairType.from_comb(
            [
                PairType.from_comb(
                    [
                        PairType.from_comb(
                            [
                                (_operator_encode(arg["administrator"])),
                                (token_id_encode(arg["all_tokens"])),
                            ]
                        ),
                        (
                            factori_types.big_map_encode(
                                factori_types.tuple2_encode(
                                    _operator_encode, token_id_encode
                                ),
                                token_id_encode,
                            )(arg["ledger"])
                        ),
                        (
                            factori_types.big_map_encode(k_encode, v_encode)(
                                arg["metadata"]
                            )
                        ),
                    ]
                ),
                PairType.from_comb(
                    [
                        (
                            factori_types.big_map_encode(
                                add_operator_encode, factori_types.unit_encode
                            )(arg["operators"])
                        ),
                        (set_pause_encode(arg["paused"])),
                    ]
                ),
                (
                    factori_types.big_map_encode(token_id_encode, type8_encode)(
                        arg["token_metadata"]
                    )
                ),
                (
                    factori_types.big_map_encode(token_id_encode, token_id_encode)(
                        arg["total_supply"]
                    )
                ),
            ]
        )
    except Exception as e:
        print("Error in storage_encode, with input:" + str(arg) + "Error:" + str(e))
        raise e


def storage_decode(arg) -> Storage:
    try:
        before_projection = (
            factori_types.tuple4_decode(
                (
                    factori_types.tuple3_decode(
                        (
                            factori_types.tuple2_decode(
                                _operator_decode, token_id_decode
                            )
                        ),
                        factori_types.big_map_decode(
                            factori_types.tuple2_decode(
                                _operator_decode, token_id_decode
                            ),
                            token_id_decode,
                        ),
                        factori_types.big_map_decode(k_decode, v_decode),
                    )
                ),
                (
                    factori_types.tuple2_decode(
                        factori_types.big_map_decode(
                            add_operator_decode, factori_types.unit_decode
                        ),
                        set_pause_decode,
                    )
                ),
                factori_types.big_map_decode(token_id_decode, type8_decode),
                factori_types.big_map_decode(token_id_decode, token_id_decode),
            )
        )(arg)
        return {
            "administrator": (before_projection[0][0][0]),
            "all_tokens": (before_projection[0][0][1]),
            "ledger": (before_projection[0][1]),
            "metadata": (before_projection[0][2]),
            "operators": (before_projection[1][0]),
            "paused": (before_projection[1][1]),
            "token_metadata": (before_projection[2]),
            "total_supply": (before_projection[3]),
        }

    except Exception as e:
        print(
            "Error in storage_decode, with input: "
            + str(arg.to_micheline_value())
            + ", and error is:"
            + str(e)
        )
        raise e


def assert_identity_storage():
    temp = storage_generator()
    temp_encoded = storage_encode(temp)
    try:
        print("++++ storage: " + str(temp_encoded.to_micheline_value()))
    except Exception as e:
        print("---- couldn't print encoded version of storage: " + str(temp))
    temp1 = storage_decode(temp_encoded)
    if temp1 == temp:
        print("storage check passed")
    else:
        if isinstance(temp, dict) and isinstance(temp1, dict):
            temp = dict(sorted(temp.items()))
            temp1 = dict(sorted(temp1.items()))
            diff_string = "\n".join(
                difflib.Differ().compare(
                    str(temp1).splitlines(), str(temp).splitlines()
                )
            )
            print(
                "storage check failed: temp = "
                + str(temp)
                + " is not equal to "
                + str(temp1)
                + "\ndiff:\n"
                + diff_string
            )


# Use this for debugging:
# try:
# 	assert_identity_storage()
# except Exception as e:
# 	print('Error in assert_identity_storage' + str(e))


def deploy(
    _from: blockchain.identity, initial_storage: Storage, amount, network, debug=False
):
    if debug:
        print("Deploying contract fa2")
    storage_encoded = storage_encode(initial_storage)
    if debug:
        print(initial_storage)
    kt1: str = blockchain.deployContract(
        "src/python_sdk/fa2_code.json",
        _from,
        storage_encoded.to_micheline_value(),
        amount,
        network,
        debug,
    )
    return kt1


def test_storage_download(kt1):
    c = pytezos.using(shell="mainnet").contract(kt1)
    return c


initial_blockchain_storage = dict(
    [
        ("administrator", "KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM"),
        ("all_tokens", 42),
        ("ledger", factori_types.BigMap(int(139881))),
        ("metadata", factori_types.BigMap(int(139882))),
        ("operators", factori_types.BigMap(int(139883))),
        ("paused", False),
        ("token_metadata", factori_types.BigMap(int(139884))),
        ("total_supply", factori_types.BigMap(int(139885))),
    ]
)  # {"prim":"Pair","args":[{"prim":"Pair","args":[{"prim":"Pair","args":[{"string":"KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM"},{"int":"42"}]},{"int":"139881"},{"int":"139882"}]},{"prim":"Pair","args":[{"int":"139883"},{"prim":"False"}]},{"int":"139884"},{"int":"139885"}]}
